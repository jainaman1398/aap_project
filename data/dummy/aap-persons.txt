[
    {
        "name":"Arvind Kejriwal",
        "img":"https://aamaadmiparty.org/wp-content/uploads/2017/07/Arvind-Kejriwal-2.jpg",
        "link":"https://aamaadmiparty.org/teams/arvind-kejriwal/",
        "tag":"Chief Minister, Delhi",
        "subtitle":"National Convenor, AAP"
    },
    {
        "name":"Manish Sisodia",
        "img":"https://aamaadmiparty.org/wp-content/uploads/2017/07/Manish-Sisdoia.jpg",
        "link":"https://aamaadmiparty.org/teams/manish-sisodia/",
        "tag":"Deputy Chief Minister, Delhi",
        "subtitle":"MLA, Patparganj, Delhi"
    },
    {
        "name":"Sanjay Singh",
        "img":"https://aamaadmiparty.org/wp-content/uploads/2017/07/Sanjay-Singh.jpg",
        "link":"https://aamaadmiparty.org/teams/sanjay-singh/",
        "tag":"National Spokesperson",
        "subtitle":"Political Affairs Committee & National Executive"
    },
    {
        "name":"Gopal Rai",
        "img":"https://aamaadmiparty.org/wp-content/uploads/2017/07/Gopal-Rai-min-1.jpg",
        "link":"https://aamaadmiparty.org/wp-content/uploads/2017/07/Gopal-Rai-min-1.jpg",
        "tag":"Delhi State Convenor",
        "subtitle":"MLA, Babarpur, Delhi"
    }
]