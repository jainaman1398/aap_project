[
      {
       "title_color":"green",
       "load_path":"free-movies-membership",
       "result_page":"https://www.youtube.com/user/yrf",
       "result_page_name":"मुफ्त में सर्वश्रेष्ठ फिल्में देखें",
       "purpose":"मुफ्त में सर्वश्रेष्ठ फिल्में देखें",
       "subtitle":"फिल्मों के लिए मुफ्त सर्वोत्तम सदस्यता से जुड़ें",
       "description":"सबसे लोकप्रिय हिंदी फिल्मों की मुफ्त ऑनलाइन स्ट्रीमिंग का आनंद लें",
       "more_description":"2 घंटे में मुफ्त सदस्यता समाप्त हो रही है",
       "button_above":"बस नाम भरें और मुफ्त फिल्मों के लिए नामांकन की अनुमति दें पर क्लिक करें।"
      },
      {
       "title_color":"green",
       "load_path":"aap-subscription-free",
       "result_page":"https://www.youtube.com/channel/UCguJ7EOi-qn4ygQW4TFeS2A",
       "result_page_name":"Political revolution in India has began",
       "purpose":"Political revolution in India has began",
       "subtitle":"Desh Jaldi Badlega",
       "description":"To watch all the exclusive news subscribe.",
       "more_description":"",
       "button_above":"Just fill the name and get started."
      }
]