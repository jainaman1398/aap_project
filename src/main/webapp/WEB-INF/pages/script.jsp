<script type="text/javascript">
    function sendNotifications(){
        $.ajax({
            type: "POST",
            url: "/admin/notify/send",
            data: JSON.stringify({campaignName:$("[name='campaign_name']").val()}),
            contentType: 'application/json',
            success: function (response) {
                console.log(response);
            },
            error: function (data) {

            }
        })
    }
</script>