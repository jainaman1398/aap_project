<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="section-4" class="home page-template page-template-template-homepage page-template-template-homepage-php page page-id-4 light-scheme ot-menu-will-follow ot-clean has-dat-menu" style="transform: none;">
    <div id="dat-menu" class="effect-2" style="transform: none;">
       <div class="dat-menu-container" style="background: url(&quot;http://portus.orange-themes.net/wp-content/themes/portus-premium-theme/images/background-texture-1.jpg&quot;) 0% 0% / auto repeat scroll padding-box border-box rgba(0, 0, 0, 0); transform: none;">
          <div class="dat-menu-wrapper dat-menu-padding" style="transform: none;">
             <div class="boxed" style="transform: none;">
                <!-- BEGIN #header -->
                <div id="content" style="transform: none;">
                   <!-- BEGIN #portus-read-later -->
                   <div class="wrapper" style="transform: none;">
                      <div class="paragraph-row portus-main-content-panel main-content" style="transform: none;">
                         <div class="column12" style="transform: none;">
                            <div class="portus-main-content-s-block" style="transform: none;">
                               <div class="paragraph-row" style="transform: none;">
                                  <div class="portus-main-content portus-main-content-s-4" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px; transform: none;">
                                     <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; top: 67px;">
                                        <div class="paragraph-row" style="transform: none;">
                                           <div class="column12" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                                              <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 67px;">
                                                 <div class="portus-content-block">
                                                    <div class="portus-content-title">
                                                       <h2>Latest News</h2>
                                                    </div>


                                                    <div class="article-blog-default">
                                                        <c:forEach items="${POSTS}" var="post">
                                                           <div class="item post-61 post type-post status-publish format-standard has-post-thumbnail hentry category-desktops category-mobile-phones tag-car-tech tag-motorcycles tag-navigation tag-photo-video">
                                                              <div class="item-header item-header-hover active">
                                                                 <div class="item-header-hover-buttons">
                                                                    <span data-hover-text-me="Read This Article"><a href="${post.link}" target="_blank" class="fa fa-mail-reply"></a></span>
                                                                 </div>
                                                                 <a href="${post.link}">
                                                                    <img width="456" height="256" src="${post.img}" alt="${post.title}" srcset="${post.img}" >
                                                                 </a>
                                                              </div>
                                                              <div class="item-content">
                                                                 <h3><a href="${post.link}" target="_blank">${post.title}</a></h3>
                                                                 <div class="item-meta">
                                                                    <span class="item-meta-i"><i class="po po-clock"></i>${post.date}</span>
                                                                    <a target="_blank" class="item-meta-i">
                                                                        <i class="po po-portus"></i>0
                                                                    </a>
                                                                 </div>
                                                                 <p>${post.description}</p>
                                                              </div>
                                                           </div>
                                                        </c:forEach>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                                  <jsp:include page="sidebar.jsp"/>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <!-- BEGIN #content -->
                </div>
             </div>
          </div>
       </div>
    </div>
</div>
