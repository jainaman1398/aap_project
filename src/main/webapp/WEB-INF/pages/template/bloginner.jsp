<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="section-2" style="margin-top:30px" class="home page-template page-template-template-homepage-v1 page-template-template-homepage-v1-php page page-id-217 kc-css-system woocommerce-js jobhunt-sticky-header-enabled  woocommerce-active wpjm-activated jobhunt">
            <div class="off-canvas-wrapper">
               <div id="page" class="hfeed site">
                  <div id="content" class="site-content" tabindex="-1">
                     <div class="container">
                        <div class="site-content-inner">
                           <div class="woocommerce"></div>
                           <div id="primary" class="content-area">
                              <main id="main" class="site-main">
                                 <section id="jh-scroll-here" class="jh-section job-categories-section v1">
                                    <div class="container">
                                       <div class="section-header">
                                          <h3 class="section-title">Popular Categories</h3>
                                          <span class="section-sub-title"></span>
                                       </div>
                                       <div class="job-categories-section__inner">
                                          <ul class="job-categories">
                                             <li class="job-category">
                                                <a href="/category">
                                                   <div class="media-icon"> <i class="la la-building-o"></i></div>
                                                   <div class="media-caption">
                                                      <h4 class="category-titile">Education</h4>
                                                      <span class="job-count"></span>
                                                   </div>
                                                </a>
                                             </li>
                                             <li class="job-category">
                                                <a href="/category">
                                                   <div class="media-icon"> <i class="la la-users"></i></div>
                                                   <div class="media-caption">
                                                      <h4 class="category-titile">Health</h4>
                                                      <span class="job-count"></span>
                                                   </div>
                                                </a>
                                             </li>
                                             <li class="job-category">
                                                <a href="/category">
                                                   <div class="media-icon"> <i class="la la-gears"></i></div>
                                                   <div class="media-caption">
                                                      <h4 class="category-titile">Electricity & Water</h4>
                                                      <span class="job-count"></span>
                                                   </div>
                                                </a>
                                             </li>
                                             <li class="job-category">
                                                <a href="/category">
                                                   <div class="media-icon"> <i class="la la-money"></i></div>
                                                   <div class="media-caption">
                                                      <h4 class="category-titile">Transport & Infrastructure</h4>
                                                      <span class="job-count"></span>
                                                   </div>
                                                </a>
                                             </li>
                                             <li class="job-category">
                                                <a href="/category">
                                                   <div class="media-icon"> <i class="la la-desktop"></i></div>
                                                   <div class="media-caption">
                                                      <h4 class="category-titile">Schemes For Your</h4>
                                                      <span class="job-count"></span>
                                                   </div>
                                                </a>
                                             </li>
                                             <li class="job-category">
                                                <a href="/category">
                                                   <div class="media-icon"> <i class="la la-line-chart"></i></div>
                                                   <div class="media-caption">
                                                      <h4 class="category-titile">Art And Culture</h4>
                                                      <span class="job-count"></span>
                                                   </div>
                                                </a>
                                             </li>
                                             <li class="job-category">
                                                <a href="/category">
                                                   <div class="media-icon"> <i class="la la-certificate"></i></div>
                                                   <div class="media-caption">
                                                      <h4 class="category-titile">Other Initiatives</h4>
                                                      <span class="job-count"></span>
                                                   </div>
                                                </a>
                                             </li>
                                             <li class="job-category">
                                                <a href="/category">
                                                   <div class="media-icon"> <i class="la la-globe"></i></div>
                                                   <div class="media-caption">
                                                      <h4 class="category-titile">AAP Organisation</h4>
                                                      <span class="job-count"></span>
                                                   </div>
                                                </a>
                                             </li>
                                          </ul>
                                          <div class="action"> <a class="action-link" href="/category">Browse All Categories</a></div>
                                       </div>
                                    </div>
                                 </section>
                              </main>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <section id="main" class="clearfix">
            <div id="content" class="site-content" role="main">
               <div id="post-33" class="post-33 page type-page status-publish hentry">
                  <div class="entry-content">
                     <div class="elementor elementor-33">
                        <div class="elementor-inner">
                           <div class="elementor-section-wrap">
                              <section data-id="3uh2bpk" class="elementor-element elementor-element-3uh2bpk elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
                                 <div class="elementor-background-overlay"></div>
                                 <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                       <div data-id="ae2mfvy" class="elementor-element elementor-element-ae2mfvy elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                          <div class="elementor-column-wrap elementor-element-populated">
                                             <div class="elementor-widget-wrap">
                                                <div data-id="eb4719c" class="elementor-element elementor-element-eb4719c elementor-widget elementor-widget-thm-title" data-element_type="thm-title.default">
                                                   <div class="elementor-widget-container">
                                                      <div class="backnow-title-content-wrapper">
                                                         <h2 class="thm-heading-title">Major Initiatives</h2>
                                                         <p class="sub-title-content"></p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div data-id="0f4772d" class="elementor-element elementor-element-0f4772d elementor-widget elementor-widget-backnow-product-tab" data-element_type="backnow-product-tab.default">
                                                   <div class="elementor-widget-container">
                                                      <div class="row">
                                                         <div class="col-12">
                                                            <ul class="trending-tabs nav themeum-tab-navigation justify-content-center">
                                                               <c:forEach items="${TRENDINGS}" var="trending">
                                                               <li class="nav-item">
                                                                  <a class="nav-link active" href="javascript:void(0)" style="margin-left:30px">${trending.tag}</a>
                                                               </li>
                                                               </c:forEach>
                                                            </ul>
                                                         </div>
                                                         <!--/.col-12-->
                                                         <div class="col-12">
                                                            <div class="tab-content" id="pills-tabContent">

                                                               <c:forEach items="${TRENDINGS}" var="trending">
                                                               <div class="tab-pane fade show active" >
                                                                  <div class="row">
                                                                     <div class="col-lg-4">
                                                                        <div class="themeum-tab-inner d-flex align-items-center" style="height: 250px;overflow:hidden">
                                                                           <div class="themeum-tab-category">
                                                                              <h3 class="crowd-cat-title"><a target="_blank" href="${trending.link}" data-hcolor="#ffc658">${trending.tag}</a></h3>
                                                                              <h6>${trending.title}</h6>
                                                                              <p>${trending.description}</p>
                                                                              <a href="${trending.link}" target="_blank" class="thm-btn" data-catbg="#ffc658" style="color:#fff;background:#ffc658;border-color:#ffc658">Read Article</a>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <!--col-lg-4-->
                                                                     <div class="col-lg-8">
                                                                        <div class="themeum-tab-inner d-flex align-items-center"  style="height: 250px;overflow:hidden">
                                                                           <div class="clearfix">
                                                                              <div class="row align-items-center">
                                                                                 <div class="col-sm-6">
                                                                                    <div class="thm-tab-content text-left">
                                                                                       <div class="themeum-campaign-tab-post">
                                                                                          <div class="themeum-campaign-post-content clearfix">
                                                                                             <h3 class="entry-title"><a target="_blank" href="${trending.link}">${trending.rtitle}</a></h3>
                                                                                             <p>${trending.rdescription} </p>
                                                                                          </div>
                                                                                       </div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <!--/.col-sm-6-->
                                                                                 <div class="col-sm-6">
                                                                                    <div class="themeum-campaign-img">
                                                                                       <a class="review-item-image" target="_blank" href="${trending.link}"><img width="600" height="580" src="${trending.img}" class="img-fluid wp-post-image" alt=""></a>
                                                                                    </div>
                                                                                 </div>
                                                                                 <!--/.col-sm-6-->
                                                                              </div>
                                                                              <!--/.row-->
                                                                           </div>
                                                                           <!--/.clearfix-->
                                                                        </div>
                                                                        <!--/.themeum-tab-inner-->
                                                                     </div>
                                                                     <!--/.col-lg-8-->
                                                                  </div>
                                                                  <!--/.row-->
                                                               </div>
                                                               <!--/.tab-pane-->
                                                               </c:forEach>

                                                            </div>
                                                            <!--/.tab-content-->
                                                         </div>
                                                         <!--/.col-12-->
                                                      </div>
                                                      <!--/.row-->
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </section>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--/#content-->
         </section>
         <!--/#main-->
         <div id="section-3" class="home page-template-default page page-id-17869 page-child parent-pageid-17326 woocommerce-js widgets_v1   navstyle-v1        header1 is_header_semitransparent  wpb-js-composer js-comp-ver-5.5.5 vc_responsive">
            <div id="page" class="hfeed site">
               <div id="primary" class="no-padding content-area no-sidebar">
                  <div class="container">
                     <div class="row">
                        <main id="main" class="col-md-12 site-main main-content">
                           <article id="post-17869" class="post-17869 page type-page status-publish hentry">
                              <div class="entry-content" id="counter">
                                 <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1534508798556 vc_row-has-fill" style="position: relative; text-align:center; box-sizing: border-box;">
                                    <div class="wpb_column vc_column_container vc_col-sm-2 vc_col-xs-6">
                                       <div class="vc_column-inner vc_custom_1534242494013">
                                          <div class="wpb_wrapper">
                                             <div class="stats-block statistics wow zoomIn  animated" style="visibility: visible; animation-name: zoomIn;">
                                                <div class="stats-img"><img src="/static/images/blog/icon1_numismatico.png" data-src="/static/images/blog/icon1_numismatico.png" alt=""></div>
                                                <div class="stats-content percentage" >
                                                   <span class="skill-count" style="color: #252525" data-perc="350">0</span>
                                                   <p style="color: rgba(37,37,37,0.6)">CLASSROOMS BUILT</p>
                                                </div>
                                             </div>
                                             <div class="vc_empty_space  spacing_mobile" style="height: 25px"><span class="vc_empty_space_inner"></span></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-2 vc_col-xs-6">
                                       <div class="vc_column-inner vc_custom_1534242503961">
                                          <div class="wpb_wrapper">
                                             <div class="stats-block statistics wow zoomIn  animated" style="visibility: visible; animation-name: zoomIn;">
                                                <div class="stats-img"><img src="/static/images/blog/icon2_numismatico.png" data-src="/static/images/blog/icon2_numismatico.png" alt=""></div>
                                                <div class="stats-content percentage">
                                                   <span class="skill-count" style="color: #252525"  data-perc="289">0</span>
                                                   <p style="color: rgba(37,37,37,0.6)">MOHALLA CLICNICS</p>
                                                </div>
                                             </div>
                                             <div class="vc_empty_space  spacing_mobile" style="height: 25px"><span class="vc_empty_space_inner"></span></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-2 vc_col-xs-6">
                                       <div class="vc_column-inner vc_custom_1534242511528">
                                          <div class="wpb_wrapper">
                                             <div class="stats-block statistics wow zoomIn  animated" style="visibility: visible; animation-name: zoomIn;">
                                                <div class="stats-img"><img src="/static/images/blog/icon3_numismatico.png" data-src="/static/images/blog/icon3_numismatico.png" alt=""></div>
                                                <div class="stats-content percentage" >
                                                   <span class="skill-count" style="color: #252525" data-perc="1233">0</span>
                                                   <p style="color: rgba(37,37,37,0.6)">OPD PATIENTS TREATED</p>
                                                </div>
                                             </div>
                                             <div class="vc_empty_space  spacing_mobile" style="height: 25px"><span class="vc_empty_space_inner"></span></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-2 vc_col-xs-6">
                                       <div class="vc_column-inner vc_custom_1534242522693">
                                          <div class="wpb_wrapper">
                                             <div class="stats-block statistics wow zoomIn  animated" style="visibility: visible; animation-name: zoomIn;">
                                                <div class="stats-img"><img src="/static/images/blog/icon4_numismatico.png" data-src="/static/images/blog/icon4_numismatico.png" alt=""></div>
                                                <div class="stats-content percentage">
                                                   <span class="skill-count" style="color: #252525"  data-perc="33">0</span>
                                                   <p style="color: rgba(37,37,37,0.6)">KL OF WATER EVERY MONTH</p>
                                                </div>
                                             </div>
                                             <div class="vc_empty_space  spacing_mobile" style="height: 25px"><span class="vc_empty_space_inner"></span></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- .entry-content -->
                           </article>
                           <!-- #post-## -->
                        </main>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="section-5" >
            <div class="home page-template page-template-page-pagebuilder-witharchive page-template-page-pagebuilder-witharchive-php page page-id-31 df-wraper group-blog wpb-js-composer js-comp-ver-5.4.2 vc_responsive" style="transform: none;">
               <div class="" style="transform: none;">
                  <div id="page" class=" df-allcontent-wrap df-content-full" style="transform: none;">
                     <div id="df-off-canvas-wrap" class="" style="transform: none;">
                        <div id="df-content-wrapper" class="lazy-wrapper df-content-full " style="transform: none;">
                           <div class="boxed boxed-modifier-wrapper no-padding" style="transform: none;">
                              <div class="container df-bg-content" style="transform: none;">
                                 <div class="row" style="transform: none;">
                                    <div class="col-md-12 df-single-page">
                                       <article id="post-31" class="post-31 page type-page status-publish">
                                          <div class="entry-content">
                                             <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid df-vc-container vc_custom_1465462167340 vc_row-no-padding df-vc-container is-stretch no--padding" style="position: relative; box-sizing: border-box; width: 1518px;">
                                                <div class="wpb_column vc_column_container vc_col-sm-12 df_col-sm-12">
                                                   <div class="vc_column-inner ">
                                                      <div class="wpb_wrapper">
                                                         <div class="df-shortcode-blocks main-carousel layout-1 full-column slick-initialized slick-slider" data-autoplay="true" data-autoplay-speed="5000">
                                                            <button type="button" data-role="none" class="slick-prev df-prev-button slick-arrow" aria-label="Previous" role="button" style="display: block;"><i class="ion-ios-arrow-left"></i></button>
                                                            <div aria-live="polite" class="slick-list draggable" style="overflow-x: scroll;" >
                                                               <div class="slick-track" style="opacity: 1; width: 3000px; left: 0px;" role="listbox"  id="style-1">
                                                                   <c:forEach items="${PERSONS}" var="person">
                                                                    <a href="${person.link}"  target="_blank" ><div class="carousel-content slick-slide slick-cloned" data-slick-index="-4" aria-hidden="true" tabindex="-1" style="width: 379px;">
                                                                         <a href="${person.link}" target="_blank" class="df-img-wrapper" tabindex="-1"><img width="960" height="640" src="${person.img}" class="img-responsive center-block article-featured-image wp-post-image" alt="" srcset="${person.img}" sizes="(max-width: 960px) 100vw, 960px"></a>
                                                                         <span class="overlay"></span>
                                                                         <div class="carousel-content-inner">
                                                                            <ul class="list-inline df-category article-category clearfix">
                                                                               <li class="entry-category">
                                                                                  <a href="${person.link}"  target="_blank" title="${person.name}" class="cat-Galleries" tabindex="-1">${person.name}</a>
                                                                               </li>
                                                                            </ul>
                                                                            <h4 class="article-title"><a href="${person.link}"  target="_blank" tabindex="-1">${person.tag}</a></h4>
                                                                            <div class="post-meta">
                                                                               <div class="post-meta-desc">
                                                                                  <a href="${person.link}" target="_blank" class="author-name" tabindex="-1">${person.subtitle}</a>
                                                                               </div>
                                                                            </div>
                                                                         </div>
                                                                        </div>
                                                                   </a>
                                                                   </c:forEach>

                                                               </div>
                                                            </div>
                                                            <button type="button" data-role="none" class="slick-next df-next-button slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="ion-ios-arrow-right"></i></button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </article>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="section-4" class="home page-template page-template-template-homepage page-template-template-homepage-php page page-id-4 light-scheme ot-menu-will-follow ot-clean has-dat-menu" style="transform: none;">
            <div id="dat-menu" class="effect-2" style="transform: none;">
               <div class="dat-menu-container" style="background: url(&quot;http://portus.orange-themes.net/wp-content/themes/portus-premium-theme/images/background-texture-1.jpg&quot;) 0% 0% / auto repeat scroll padding-box border-box rgba(0, 0, 0, 0); transform: none;">
                  <div class="dat-menu-wrapper dat-menu-padding" style="transform: none;">
                     <div class="boxed" style="transform: none;">
                        <!-- BEGIN #header -->
                        <div id="content" style="transform: none;">
                           <!-- BEGIN #portus-read-later -->
                           <div class="wrapper" style="transform: none;">
                              <div class="paragraph-row portus-main-content-panel main-content" style="transform: none;">
                                 <div class="column12" style="transform: none;">
                                    <div class="portus-main-content-s-block" style="transform: none;">
                                       <div class="paragraph-row" style="transform: none;">
                                          <div class="portus-main-content portus-main-content-s-4" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px; transform: none;">
                                             <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; top: 67px;">
                                                <div class="paragraph-row" style="transform: none;">
                                                   <div class="column12" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                                                      <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 67px;">
                                                         <div class="portus-content-block">
                                                            <div class="portus-content-title">
                                                               <h2>Latest News</h2>
                                                            </div>


                                                            <div class="article-blog-default">
                                                                <c:forEach items="${POSTS}" var="post">
                                                                   <div class="item post-61 post type-post status-publish format-standard has-post-thumbnail hentry category-desktops category-mobile-phones tag-car-tech tag-motorcycles tag-navigation tag-photo-video">
                                                                      <div class="item-header item-header-hover active">
                                                                         <div class="item-header-hover-buttons">
                                                                            <span data-hover-text-me="Read This Article"><a href="${post.link}" target="_blank" class="fa fa-mail-reply"></a></span>
                                                                         </div>
                                                                         <a href="${post.link}">
                                                                            <img width="456" height="256" src="${post.img}" alt="${post.title}" srcset="${post.img}" >
                                                                         </a>
                                                                      </div>
                                                                      <div class="item-content">
                                                                         <h3><a href="${post.link}" target="_blank">${post.title}</a></h3>
                                                                         <div class="item-meta">
                                                                            <span class="item-meta-i"><i class="po po-clock"></i>${post.date}</span>
                                                                            <a target="_blank" class="item-meta-i">
                                                                                <i class="po po-portus"></i>0
                                                                            </a>
                                                                         </div>
                                                                         <p>${post.description}</p>
                                                                      </div>
                                                                   </div>
                                                                </c:forEach>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <jsp:include page="sidebar.jsp"/>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- BEGIN #content -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
               <script>
                 var a = 0;
                 function handleScroll(){
                    var oTop = $('#counter').offset().top - window.innerHeight;
                       if (a == 0 && $(window).scrollTop() > oTop) {
                         $('.skill-count').each(function() {
                           var $this = $(this),
                             countTo = $this.attr('data-perc');
                           $({
                             countNum: $this.text()
                           }).animate({
                               countNum: countTo
                             },
                             {
                               duration: 2000,
                               easing: 'swing',
                               step: function() {
                                 $this.text(Math.floor(this.countNum));
                               },
                               complete: function() {
                                 $this.text(this.countNum);
                                 //alert('finished');
                               }
    
                             });
                         });
                         a = 1;
                       }
                 }
                 
                 $(window).scroll(function() {
                   handleScroll();
                 });
                   handleScroll();

                 $(".search-open-icon").click(function(){
                     $(".thm-fullscreen-search").addClass("active");
                 });
                 $(".search-overlay").click(function(){
                     $(".thm-fullscreen-search").removeClass("active");
                 });

                $(".trending-tabs .nav-link").removeClass("active");
                $($(".trending-tabs .nav-link")[0]).addClass("active");

                 $(".trending-tabs .nav-link").click(function(){
                    $(".trending-tabs .nav-link").removeClass("active");
                    $(this).addClass("active");
                    var index = $(".trending-tabs .nav-link").index(this);

                    showTabNumber(index);

                 });

                function showTabNumber(num){

                $("#pills-tabContent .tab-pane").removeClass("active");
                $($("#pills-tabContent .tab-pane")[num]).addClass("active");

                $("#pills-tabContent .tab-pane").removeClass("show");
                $($("#pills-tabContent .tab-pane")[num]).addClass("show");
                }

                showTabNumber(0);
               </script>
