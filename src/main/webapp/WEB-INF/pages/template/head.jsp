<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head>
      <style type="text/css">
         .thm-fullscreen-search.active {
            opacity: 1;
            visibility: visible;
         }
         @media all{
         .float-left{float:left;}
         .float-right{float:right;}
         .text-right{text-align:right;}
         }

        @media (min-width: 768px)
        #section-3 .vc_col-sm-3 {
            width: 24.666667%;
        }
         @media all{
         .float-left{float:left;}
         .float-right{float:right;}
         }

         @media all{
         button:hover{cursor:pointer;}
         }

         @media all{
         @media print{
         *,::after,::before{text-shadow:none!important;box-shadow:none!important;}
         a,a:visited{text-decoration:underline;}
         img{page-break-inside:avoid;}
         h2,h3,p{orphans:3;widows:3;}
         h2,h3{page-break-after:avoid;}
         }
         *,::after,::before{box-sizing:border-box;}
         footer,header,section{display:block;}
         body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff;}
         [tabindex="-1"]:focus{outline:0!important;}
         h2,h3,h4,h6{margin-top:0;margin-bottom:.5rem;}
         p{margin-top:0;margin-bottom:1rem;}
         ul{margin-top:0;margin-bottom:1rem;}
         ul ul{margin-bottom:0;}
         a{color:#007bff;text-decoration:none;background-color:transparent;-webkit-text-decoration-skip:objects;}
         a:hover{color:#0056b3;text-decoration:underline;}
         img{vertical-align:middle;border-style:none;}
         a,button,input:not([type=range]),label{-ms-touch-action:manipulation;touch-action:manipulation;}
         label{display:inline-block;margin-bottom:.5rem;}
         button{border-radius:0;}
         button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color;}
         button,input{margin:0;font-family:inherit;font-size:inherit;line-height:inherit;}
         button,input{overflow:visible;}
         button{text-transform:none;}
         [type=submit],button,html [type=button]{-webkit-appearance:button;}
         [type=button]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{padding:0;border-style:none;}
         h2,h3,h4,h6{margin-bottom:.5rem;font-family:inherit;font-weight:500;line-height:1.2;color:inherit;}
         h2{font-size:2rem;}
         h3{font-size:1.75rem;}
         h4{font-size:1.5rem;}
         h6{font-size:1rem;}
         .lead{font-size:1.25rem;font-weight:300;}
         .img-fluid{max-width:100%;height:auto;}
         .container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;}
         @media (min-width:576px){
         .container{max-width:540px;}
         }
         @media (min-width:768px){
         .container{max-width:720px;}
         }
         @media (min-width:992px){
         .container{max-width:960px;}
         }
         @media (min-width:1200px){
         .container{max-width:1140px;}
         }
         .row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px;}
         .col-12,.col-4,.col-5,.col-7,.col-8,.col-lg,.col-lg-4,.col-lg-5,.col-lg-8,.col-lg-auto,.col-md-6,.col-sm-6{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px;}
         .col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%;}
         .col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%;}
         .col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%;}
         .col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%;}
         .col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%;}
         @media (min-width:576px){
         .col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%;}
         }
         @media (min-width:768px){
         .col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%;}
         }
         @media (min-width:992px){
         .col-lg{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%;}
         .col-lg-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none;}
         .col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%;}
         .col-lg-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%;}
         .col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%;}
         .order-lg-1{-ms-flex-order:1;order:1;}
         .order-lg-3{-ms-flex-order:3;order:3;}
         }
         .form-control{display:block;width:100%;padding:.375rem .75rem;font-size:1rem;line-height:1.5;color:#495057;background-color:#fff;background-image:none;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;}
         .form-control::-ms-expand{background-color:transparent;border:0;}
         .form-control:focus{color:#495057;background-color:#fff;border-color:#80bdff;outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25);}
         .form-control::-webkit-input-placeholder{color:#868e96;opacity:1;}
         .form-control:-ms-input-placeholder{color:#868e96;opacity:1;}
         .form-control::-ms-input-placeholder{color:#868e96;opacity:1;}
         .form-control::placeholder{color:#868e96;opacity:1;}
         .form-control:disabled{background-color:#e9ecef;opacity:1;}
         .btn{display:inline-block;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;}
         .btn:focus,.btn:hover{text-decoration:none;}
         .btn:focus{outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25);}
         .btn:disabled{opacity:.65;}
         .btn:not([disabled]):not(.disabled):active{background-image:none;}
         .btn-primary{color:#fff;background-color:#007bff;border-color:#007bff;}
         .btn-primary:hover{color:#fff;background-color:#0069d9;border-color:#0062cc;}
         .btn-primary:focus{box-shadow:0 0 0 .2rem rgba(0,123,255,.5);}
         .btn-primary:disabled{background-color:#007bff;border-color:#007bff;}
         .btn-primary:not([disabled]):not(.disabled):active{color:#fff;background-color:#0062cc;border-color:#005cbf;box-shadow:0 0 0 .2rem rgba(0,123,255,.5);}
         .fade{opacity:0;transition:opacity .15s linear;}
         .fade.show{opacity:1;}
         .collapse{display:none;}
         .nav{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding-left:0;margin-bottom:0;list-style:none;}
         .nav-link{display:block;padding:.5rem 1rem;}
         .nav-link:focus,.nav-link:hover{text-decoration:none;}
         .tab-content>.tab-pane{display:none;}
         .tab-content>.active{display:block;}
         .navbar-nav{display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;padding-left:0;margin-bottom:0;list-style:none;}
         .navbar-collapse{-ms-flex-preferred-size:100%;flex-basis:100%;-ms-flex-positive:1;flex-grow:1;-ms-flex-align:center;align-items:center;}
         .alert{position:relative;padding:.75rem 1.25rem;margin-bottom:1rem;border:1px solid transparent;border-radius:.25rem;}
         .alert-danger{color:#721c24;background-color:#f8d7da;border-color:#f5c6cb;}
         .progress{display:-ms-flexbox;display:flex;height:1rem;overflow:hidden;font-size:.75rem;background-color:#e9ecef;border-radius:.25rem;}
         .progress-bar{display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;-ms-flex-pack:center;justify-content:center;color:#fff;background-color:#007bff;}
         .close{float:right;font-size:1.5rem;font-weight:700;line-height:1;color:#000;text-shadow:0 1px 0 #fff;opacity:.5;}
         .close:focus,.close:hover{color:#000;text-decoration:none;opacity:.75;}
         button.close{padding:0;background:0 0;border:0;-webkit-appearance:none;}
         .modal{position:fixed;top:0;right:0;bottom:0;left:0;z-index:1050;display:none;overflow:hidden;outline:0;}
         .modal.fade .modal-dialog{transition:-webkit-transform .3s ease-out;transition:transform .3s ease-out;transition:transform .3s ease-out,-webkit-transform .3s ease-out;-webkit-transform:translate(0,-25%);transform:translate(0,-25%);}
         .modal-dialog{position:relative;width:auto;margin:10px;pointer-events:none;}
         .modal-content{position:relative;display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;pointer-events:auto;background-color:#fff;background-clip:padding-box;border:1px solid rgba(0,0,0,.2);border-radius:.3rem;outline:0;}
         .modal-header{display:-ms-flexbox;display:flex;-ms-flex-align:start;align-items:flex-start;-ms-flex-pack:justify;justify-content:space-between;padding:15px;border-bottom:1px solid #e9ecef;border-top-left-radius:.3rem;border-top-right-radius:.3rem;}
         .modal-header .close{padding:15px;margin:-15px -15px -15px auto;}
         .modal-title{margin-bottom:0;line-height:1.5;}
         .modal-body{position:relative;-ms-flex:1 1 auto;flex:1 1 auto;padding:15px;}
         .modal-footer{display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;-ms-flex-pack:end;justify-content:flex-end;padding:15px;border-top:1px solid #e9ecef;}
         @media (min-width:576px){
         .modal-dialog{max-width:500px;margin:30px auto;}
         }
         .clearfix::after{display:block;clear:both;content:"";}
         .d-none{display:none!important;}
         .d-inline-block{display:inline-block!important;}
         .d-block{display:block!important;}
         .d-flex{display:-ms-flexbox!important;display:flex!important;}
         @media (min-width:992px){
         .d-lg-none{display:none!important;}
         .d-lg-block{display:block!important;}
         }
         .flex-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important;}
         .justify-content-center{-ms-flex-pack:center!important;justify-content:center!important;}
         .align-items-center{-ms-flex-align:center!important;align-items:center!important;}
         .float-left{float:left!important;}
         .float-right{float:right!important;}
         .text-left{text-align:left!important;}
         .text-right{text-align:right!important;}
         }

         @media all{
         .fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
         .pull-right{float:right;}
         .pull-left{float:left;}
         .fa-search:before{content:"\f002";}
         .fa-twitter:before{content:"\f099";}
         .fa-facebook:before{content:"\f09a";}
         .fa-navicon:before{content:"\f0c9";}
         .fa-google-plus:before{content:"\f0d5";}
         .fa-linkedin:before{content:"\f0e1";}
         .fa-angle-right:before{content:"\f105";}
         .fa-angle-down:before{content:"\f107";}
         .fa-dribbble:before{content:"\f17d";}
         .fa-behance:before{content:"\f1b4";}
         }

         @media all{
         [class^="back-"]{font-family:'backnow'!important;speak:none;font-style:normal;font-weight:normal;font-variant:normal;text-transform:none;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
         .back-layout:before{content:"\e929";}
         .back-magnifying-glass-2:before{content:"\e92b";}
         .back-profile:before{content:"\e93e";}
         .back-photo-camera2:before{content:"\e988";}
         .back-video-camera2:before{content:"\e989";}
         .back-robot-head:before{content:"\e98a";}
         .back-scissors:before{content:"\e98b";}
         .back-music-note-black-symbol:before{content:"\e98c";}
         .back-random-line:before{content:"\e98d";}
         .back-gamepad2:before{content:"\e98e";}
         .back-fast-food:before{content:"\e98f";}
         .back-hand-mic:before{content:"\e990";}
         .back-polo-shirt:before{content:"\e991";}
         }

         @media all{
         button::-moz-focus-inner{padding:0;border:0;}
         }

         @media all{
         .thm-btn,.btn{line-height:43px;padding:0 25px;color:#fff;display:inline-block;border-radius:3px;-webkit-transition:.4s;transition:.4s;border:1px solid;}
         .thm-btn,input[type=submit]{color:#fff;cursor:pointer;}
         .thm-btn:hover,input[type=submit]:hover{background:#fff;}
         .backnow-login-register a.backnow-dashboard{padding:5px 15px;border-radius:4px;color:#fff;transition:400ms;-webkit-transition:400ms;}
         .btn:focus,input:focus,button:focus{outline-offset:0;outline:0;-webkit-box-shadow:none;box-shadow:none;}
         .entry-content .elementor-widget:not(:last-child){margin-bottom:20px;}
         .backnow-title-content-wrapper{margin-bottom:60px;}
         h2.thm-heading-title{font-weight:300;}
         .sub-title-content{font-size:20px;font-weight:300;color:#222;margin-bottom:0;}
         .thm-progress-bar .progress-bar{color:#fff;height:6px;-webkit-box-shadow:none;box-shadow:none;margin-bottom:0;border-radius:10px;}
         .themeum-campaign-img{margin:-1px -1px 0;position:relative;overflow:hidden;}
         .themeum-campaign-img .review-item-image img{border-radius:5px;}
         .themeum-tab-category p{font-weight:300;}
         .progressbar-content-wrapper{padding:0 20px;}
         .progressbar-content-wrapper .thm-progress-bar .progress{height:6px;background-color:#eee;-webkit-box-shadow:none;box-shadow:none;border-radius:10px;margin-top:12px;margin-bottom:12px;}
         .progressbar-content-wrapper .thm-progress-bar .thm-raise-sp{color:#8A8D91;font-weight:300;}
         .progressbar-content-wrapper .thm-progress-bar .lead{font-size:14px;font-weight:500;margin:0;color:#393939;}
         .progressbar-content-wrapper .thm-meta-desc{width:50%;display:inline-block;}
         .themeum-camp-author{padding:20px;}
         .themeum-author-img{height:40px;width:40px;border-radius:50%;overflow:hidden;}
         .themeum-author-img img{width:100%;}
         .themeum-author-dsc{padding-left:10px;}
         .themeum-author-dsc span,.themeum-author-funded span{display:block;color:#AAAEB3;font-size:14px;font-weight:300;line-height:1;display:block;}
         .themeum-author-dsc a,.themeum-author-funded h6{font-size:14px;line-height:1;color:#363636;display:block;margin-bottom:6px;margin-top:3px;}
         .themeum-author-funded{text-align:right;}
         .social-share,.copy-wrapper{padding-top:2px;text-align:center;}
         .social-share ul{margin:0;padding:0;}
         .form-control{border-radius:2px;-webkit-box-shadow:none;box-shadow:none;border:1px solid #f2f2f2;}
         .footer-copyright{color:#dedede;}
         .footer-copyright span{color:#dedede;}
         .thm-explore{position:relative;padding-right:10px;}
         .thm-explore::after{content:'';position:absolute;height:16px;width:1px;background:#DCDCDC;right:-10px;top:50%;margin-top:-8px;}
         .thm-explore ul{position:absolute;list-style:none;background:#fff;top:65px;left:0;z-index:9;min-width:210px;padding:15px 0;border-radius:2px;overflow:hidden;-webkit-transition:all .4s ease .2s;transition:all .4s ease .2s;opacity:0;visibility:hidden;-webkit-box-shadow:0 3px 5px 0 rgba(0, 0, 0, 0.08);box-shadow:0 3px 5px 0 rgba(0, 0, 0, 0.08);}
         .thm-explore:hover ul{opacity:1;visibility:visible;-webkit-transition-delay:0s;transition-delay:0s;}
         .thm-explore ul li a{color:#888;padding:5px 20px;display:block;line-height:26px;-webkit-transition:.4s;transition:.4s;}
         .thm-explore ul li a i{margin-right:10px;font-size:17px;line-height:26px;}
         .thm-explore>a{padding:12px 0;line-height:30px;}
         .thm-explore>a i:first-child{margin-right:5px;}
         .thm-explore>a i:last-child{margin-left:5px;}
         .site-header{background:#fff;padding:20px 0;border-bottom:1px solid #EBEBF1;}
         .common-menu-wrap{position:inherit;text-align:right;}
         .common-menu-wrap .nav>li{display:inline-block;position:relative;padding:0;margin-left:28px;}
         .thm-explore>a,.common-menu-wrap .nav>li>a{display:inline-block;line-height:30px;padding:0;font-size:14px;font-weight:300;color:#676767;position:relative;padding:12px 0;transition:400ms;-webkit-transition:400ms;}
         .logo-wrapper{padding:0;}
         .logo-wrapper a{display:inline-block;padding:14px 0;}
         .common-menu-wrap .nav>li>a:hover,.common-menu-wrap .nav>li>a:focus{background-color:transparent;}
         .common-menu-wrap .nav>li.menu-item-has-children{position:relative;z-index:100;}
         .common-menu-wrap .nav>li.menu-item-has-children>a{position:relative;}
         .common-menu-wrap .nav>li.menu-item-has-children>a:after{content:" \e916";font-family:'backnow'!important;margin-left:0;color:#191919;font-size:9px;position:relative;top:-1px;}
         .header-solid .common-menu-wrap .nav>li.menu-item-has-children:after{color:#000;}
         .common-menu-wrap .nav>li>ul{padding:0px 0 0;}
         .common-menu-wrap .nav>li ul{text-align:left;position:absolute;display:none;left:0;list-style:none;margin:0;width:220px;z-index:999999;color:#fff;top:100%;-webkit-box-shadow:0 3px 5px 0 rgba(0, 0, 0, 0.08);box-shadow:0 3px 5px 0 rgba(0, 0, 0, 0.08);border-radius:0 0 3px 3px;-webkit-transition:400ms;transition:400ms;margin-top:10px;-webkit-transition:all .4s ease .2s;transition:all .4s ease .2s;}
         .common-menu-wrap .nav>li ul::after{position:absolute;bottom:100%;left:0;height:10px;width:100%;content:'';background-color:transparent;}
         .common-menu-wrap .nav>li:hover>ul{padding:10px 0;display:block;-webkit-transition:.4s;transition:.4s;}
         .common-menu-wrap .nav>li ul{background-color:#fff;}
         .common-menu-wrap .nav>li>ul li a{display:block;line-height:normal;padding:2px 20px;font-size:14px;color:#000;transition:400ms;-webkit-transition:400ms;}
         .common-menu-wrap .nav>li>ul li:last-child a{border-bottom-width:0;border-radius:0 0 3px 3px;}
         .common-menu-wrap .nav>li>ul li a:hover{text-decoration:none;color:#fff;}
         .common-menu-wrap .nav>li>ul{transition:300ms;-webkit-transition:300ms;}
         .common-menu-wrap .nav>li>ul li{transition:300ms;-webkit-transition:300ms;}
         .common-menu-wrap .nav>li>ul li{position:relative;transiton:300ms;-webkit-transiton:300ms;-moz-transiton:300ms;}
         #mobile-menu{position:absolute;top:70px;width:100%;z-index:9;right:0;z-index:999;background:#eeeeee;min-width:220px;}
         .header-solid #mobile-menu{top:100%;}
         .navbar-toggle{border:none;margin:0;z-index:3;font-size:30px;text-align:right;background:transparent;padding-right:0;line-height:26px;padding:12px 5px;font-weight:300;color:#676767;}
         #mobile-menu .navbar-collapse{border:0;}
         #mobile-menu .navbar-nav{margin-top:10px;margin-bottom:10px;}
         #mobile-menu ul{list-style:none;}
         #mobile-menu ul li{margin-bottom:1px;}
         #mobile-menu ul li a{padding-top:8px;padding-bottom:8px;padding-left:20px;padding-right:20px;color:#676767;display:block;font-weight:300;}
         #mobile-menu ul li.active>a,#mobile-menu ul li a:hover{background:none;}
         #mobile-menu ul ul{padding-left:10px;padding-top:0;padding-bottom:0;}
         #mobile-menu ul li span.menu-toggler{display:inline-block;width:34px;cursor:pointer;color:#000;height:34px;line-height:34px;text-align:center;position:absolute;right:0;top:0;z-index:99;}
         #mobile-menu ul li:hover .menu-toggler{color:#222;}
         #mobile-menu ul li{line-height:20px;display:block;position:relative;}
         #mobile-menu ul li span.menu-toggler i{display:block;line-height:44px;}
         #mobile-menu ul li span.menu-toggler i.fa-angle-right:before{content:"\f107";}
         #mobile-menu ul li span.menu-toggler.collapsed i.fa-angle-right:before{content:"\f105";}
         #mobile-menu ul li span.menu-toggler.collapsed .fa-angle-right{display:block;}
         .backnow-search-wrap{display:inline-block;}
         .thm-fullscreen-search{position:fixed;height:100%;width:100%;top:0;left:0;z-index:9999;visibility:hidden;opacity:0;-webkit-transition:.4s;-o-transition:.4s;transition:.4s;}
         .thm-fullscreen-search .search-overlay{background-color:#000;position:absolute;height:100%;width:100%;left:0;top:0;z-index:-1;opacity:.85;cursor:url(/static/images/common/close_1.png), auto;}
         .thm-fullscreen-search form{position:relative;width:50%;}
         .thm-fullscreen-search form input[type="text"]{width:100%;border:none;border-bottom:1px solid #fff;height:55px;background-color:transparent;font-size:25px;color:#fff;font-weight:300;}
         .thm-fullscreen-search form input[type="text"]::-webkit-input-placeholder{color:#fff;}
         .thm-fullscreen-search form input[type="text"]::-moz-placeholder{color:#fff;}
         .thm-fullscreen-search form input[type="text"]:-ms-input-placeholder{color:#fff;}
         .thm-fullscreen-search form input[type="text"]:-moz-placeholder{color:#fff;}
         .thm-fullscreen-search label{position:absolute;right:0;top:0;line-height:55px;font-size:18px;color:#fff;text-transform:uppercase;}
         .themeum-navbar-header{display:inline-block;}
         .backnow-login-register{position:relative;float:right;padding:10px 0;}
         .backnow-login-register ul{list-style:none;padding:0;margin:0 0 0 10px;position:relative;display:inline-block;position:relative;}
         .backnow-login-register ul::before{content:'';position:absolute;left:0;top:50%;background:#DCDCDC;width:1px;height:15px;margin-top:-7.5px;}
         .backnow-login-register ul li{display:inline-block;position:relative;padding:0;margin-left:15px;}
         .backnow-login-register ul li a{display:block;position:relative;font-size:14px;font-weight:300;color:#676767;}
         .backnow-login-register ul li a i{margin-right:5px;color:#B2B2B2;}
         .backnow-login-register a.backnow-dashboard{text-transform:uppercase;}
         .modal{text-align:center;}
         .modal .modal-dialog{max-width:650px;}
         .modal .modal-content{border-radius:4px;background-color:#fff;margin-top:100px;text-align:center;}
         .modal .modal-content .modal-body input:not([type="checkbox"]){height:45px;padding:0;font-size:14px;font-weight:300;display:block;margin:0 auto;}
         .modal .modal-content .modal-body input[type="submit"]{padding-left:25px;padding-right:25px;margin-top:5px;}
         .modal .modal-content .modal-body input[type="submit"]:hover{background-color:#fff!important;}
         .modal .modal-header{padding:80px 80px 0px 80px;border-bottom:none;border-radius:4px;display:block;}
         .modal .modal-body{padding:0px 80px 80px 80px;}
         .modal .modal-title{color:#313232;font-size:22px;font-size:48px;text-align:center;font-weight:300;}
         .modal-header .close{position:absolute;right:30px;top:30px;}
         .modal .modal-text{font-size:16px;line-height:26px;margin-bottom:45px;}
         .modal button.close{filter:alpha(opacity=60);opacity:.6;background:none;}
         .modal button.close span{font-size:20px;font-size:15px;border-radius:50%;border:1px solid #B2B2B2;color:#999;width:30px;height:30px;line-height:30px;display:inline-block;height:30px;background:#fff;}
         .modal input#username,.modal input#password,.modal input#email{border-radius:3px;-webkit-box-shadow:none;box-shadow:none;padding:10px 20px;height:auto;margin-bottom:14px;border-color:#f1f1f1;padding:14px 25px 14px 25px;padding:0.77777778rem 1.38888889rem 0.77777778rem 1.38888889rem;font-size:14px;font-size:0.77777778rem;}
         .modal input#password{margin-bottom:15px;}
         .modal .submit_button{position:relative;}
         .modal-footer{padding-left:35px;padding-right:35px;}
         .themeum-tab-inner{text-align:center;border-radius:4px;padding:20px 30px;background-color:#fff;border:1px solid #EBEBF1;}
         .themeum-campaign-tab-post .progressbar-content-wrapper{padding:0;border-bottom-color:transparent;}
         .themeum-campaign-tab-post .themeum-camp-author{padding:0;}
         .themeum-tab-inner .themeum-campaign-post-content,.themeum-tab-category{color:#A0A0A0;font-weight:400;padding-top:15px;padding-bottom:15px;}
         .themeum-tab-category .thm-cat-icon{font-size:85px;line-height:1;max-height:85px;margin-bottom:10px;}
         .themeum-tab-inner .themeum-campaign-post-content h3,.themeum-tab-category h3{font-size:22px;font-weight:400;margin-bottom:15px;line-height:30px;}
         .themeum-tab-inner .themeum-campaign-post-content h3 a,.themeum-tab-category h3 a{color:#353535;-webkit-transition:.4s;transition:.4s;}
         .themeum-tab-category h3 a:hover{opacity:.6;}
         .themeum-tab-inner .themeum-campaign-post-content p{font-weight:300;}
         .themeum-tab-category h6{font-weight:400;margin-bottom:20px;}
         .themeum-tab-category a.thm-btn{margin-top:10px;}
         .themeum-tab-navigation{margin-bottom:20px;}
         .themeum-tab-navigation li a{font-size:16px;font-weight:300;color:#9C9CA0;margin-bottom:10px;line-height:35px;padding:0 15px;}
         .themeum-tab-navigation li a.active{background-color:#fff;color:#33d3c0;border-radius:4px;border:1px solid #EBEBF1;}
         .themeum-campaign-tab-post .themeum-camp-author{padding-top:12px;}
         img{max-width:100%;height:auto;}
         }

         @media all{
         input[type="text"],input[type="password"]{font-size:16px;}
         }

         @media all{
         .social-share ul li{display:inline-block;text-align:right;padding:0 0 0px 15px;}
         .copy-wrapper .social-share{text-align:right;}
         .footer-copyright img.enter-logo{margin-right:10px;margin-top:-8px;}
         .footer-copyright span{font-size:14px;font-weight:300;}
         .footer-copyright{margin-top:0;}
         }

         @media all{
         @media (min-width: 980px) and (max-width: 1400px){
         .common-menu-wrap .nav>li{margin-left:15px;}
         }
         @media (min-width: 768px) and (max-width: 1000px){
         .themeum-tab-category{margin:0 auto;}
         .navbar-collapse.collapse{display:none!important;overflow:hidden!important;}
         .navbar-collapse{padding-right:0;padding-left:0;}
         #mobile-menu ul{padding:0;}
         .header-solid #mobile-menu{top:100%;}
         .navbar-toggle{display:inline-block;}
         }
         @media only screen and (max-width: 992px){
         .themeum-author-dsc{text-align:left;}
         .navbar-collapse{width:auto;border-top:0;-webkit-box-shadow:none;box-shadow:none;}
         .navbar-toggle{display:inline-block;margin-top:0;outline:none;right:0;font-size:15px;padding-left:15px;}
         .navbar-nav{float:none;}
         .navbar-nav>li{float:none;}
         }
         @media (max-width: 767px){
         .themeum-campaign-tab-post .themeum-camp-author{margin-bottom:20px;}
         .thm-fullscreen-search form{width:70%;}
         .modal .modal-header{padding:35px 35px 0 35px;}
         .modal .modal-body{padding:0 35px 35px 35px;}
         .thm-explore::after{display:none;}
         .col-8 .backnow-login-register ul::before{content:none;}
         .backnow-login-register a.backnow-dashboard{font-size:11px;}
         #mobile-menu ul{padding:0;}
         .header-solid #mobile-menu{top:100%;}
         }
         @media (min-width: 768px){
         .navbar-nav{float:none;margin:0;}
         .navbar-nav>li{float:none;}
         }
         @media (max-width: 489px){
         .thm-fullscreen-search form{width:85%;}
         .thm-fullscreen-search form input[type="text"]{font-size:20px;}
         .social-share,.copy-wrapper{margin-bottom:15px;text-align:center;}
         .social-share,.copy-wrapper{padding-top:8px;text-align:center!important;}
         .copy-wrapper span{display:block;margin-top:5px;}
         }
         }
         @media all{
         a{text-decoration:none!important;}
         a:focus{outline:0;outline-offset:0;}
         ul li{padding:5px 0;}
         }

         a,#mobile-menu ul li.active>a,#mobile-menu ul li a:hover,.thm-btn:hover,input[type=submit]:hover,.themeum-tab-category .thm-cat-icon,.themeum-tab-inner .themeum-campaign-post-content h3 a:hover,.thm-explore ul li a:hover{color:#33d3c0;}
         .thm-progress-bar .progress-bar,.progressbar-content-wrapper .thm-progress-bar .progress .progress-bar,.thm-btn,.btn{background:#33d3c0;}
         .modal .modal-content .modal-body input[type="submit"]:hover{color:#33d3c0!important;}
         input:focus{border-color:#33d3c0;}
         .thm-btn,.btn,.modal .modal-content .modal-body input[type="submit"]{background-color:#33d3c0;border-color:#33d3c0;}
         .progressbar-content-wrapper .thm-progress-bar .progress .progress-bar{background:#33d3c0;background:-moz-linear-gradient(left, #00bf9c 0%, #33d3c0 100%);background:-webkit-linear-gradient(left, #00bf9c 0%,#33d3c0 100%);background:linear-gradient(to right, #00bf9c 0%,#33d3c0 100%);}
         a:hover,.footer-copyright a:hover{color:#00bf9c;}
         .btn.btn-primary:hover{background-color:#00bf9c;}
         body{font-size:14px;font-family:Montserrat;font-weight:300;line-height:24px;color:#979aa1;}
         .common-menu-wrap .nav>li>a,.thm-explore ul li a,.thm-explore a,.common-menu-wrap .nav> li > ul li a{font-size:14px;font-family:Montserrat;font-weight:300;line-height:30px;color:#676767;}
         h2{font-size:36px;font-family:Montserrat;font-weight:400;line-height:42px;color:#414040;}
         h3{font-size:26px;font-family:Montserrat;font-weight:400;line-height:28px;color:#414040;}
         h4{font-size:18px;font-family:Montserrat;font-weight:600;line-height:26px;color:#414040;}
         .site-header{background:#fff;}
         .site-header{padding-top:10px;}
         .site-header{padding-bottom:10px;}
         .themeum-navbar-header .themeum-navbar-brand img{width:130px;max-width:none;}
         body{background-color:#ffffff;}
         input[type=submit]{background-color:#33d3c0;border-color:#33d3c0;color:#fff!important;}
         .backnow-login-register a.backnow-dashboard{background-color:#0074b0;}
         input[type=submit]:hover{background-color:#33d3c0;border-color:#33d3c0;color:#fff!important;}
         .header-solid .common-menu-wrap .nav>li.menu-item-has-children:after,.header-solid .common-menu-wrap .nav>li>a,.header-solid .common-menu-wrap .nav>li>a:after,.backnow-search{color:#676767;}
         .header-solid .common-menu-wrap .nav>li>a:hover,.header-solid .common-menu-wrap .nav>li>a:hover:after,.backnow-search-wrap a.backnow-search:hover{color:#33d3c0;}
         .header-solid .common-menu-wrap .nav>li.active>a{color:#33d3c0;}
         .common-menu-wrap .nav>li ul{background-color:#fff;}
         .common-menu-wrap .nav>li>ul li a{color:#676767;border-color:#eef0f2;}
         .common-menu-wrap .nav>li>ul li a:hover,.common-menu-wrap .nav>li.current-menu-item.menu-item-has-children > a:after{color:#33d3c0;}
         .common-menu-wrap .nav>li > ul::after{border-color:transparent transparent #262626 transparent;}
         #footer{background-color:#26262b;}
         #footer{padding-top:30px;}
         #footer{padding-bottom:30px;}
         .footer-copyright,.footer-copyright a{color:#6e6e6e;}

         @media all{
         .elementor{-webkit-hyphens:manual;-ms-hyphens:manual;hyphens:manual;}
         .elementor *,.elementor :after,.elementor :before{-webkit-box-sizing:border-box;box-sizing:border-box;}
         .elementor a{-webkit-box-shadow:none;box-shadow:none;text-decoration:none;}
         .elementor img{height:auto;max-width:100%;border:none;-webkit-border-radius:0;border-radius:0;-webkit-box-shadow:none;box-shadow:none;}
         .elementor .elementor-background-overlay{height:100%;width:100%;top:0;left:0;position:absolute;}
         .elementor:after{position:absolute;opacity:0;width:0;height:0;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;}
         @media (min-width:1025px){
         .elementor:after{content:"desktop";}
         }
         @media (min-width:768px) and (max-width:1024px){
         .elementor:after{content:"tablet";}
         }
         @media (max-width:767px){
         .elementor:after{content:"mobile";}
         }
         .elementor-section{position:relative;}
         .elementor-section .elementor-container{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;margin-right:auto;margin-left:auto;position:relative;}
         .elementor-section.elementor-section-boxed>.elementor-container{max-width:1140px;}
         .elementor-row{width:100%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;}
         @media (max-width:1024px){
         .elementor-row{-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;}
         }
         .elementor-column-wrap{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;}
         .elementor-column-wrap,.elementor-widget-wrap{width:100%;position:relative;}
         .elementor-widget{position:relative;}
         .elementor-widget:not(:last-child){margin-bottom:20px;}
         .elementor-column{position:relative;min-height:1px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;}
         .elementor-column-gap-default>.elementor-row>.elementor-column>.elementor-element-populated{padding:10px;}
         @media (min-width:768px){
         .elementor-column.elementor-col-100{width:100%;}
         }
         @media (max-width:767px){
         .elementor-column{width:100%;}
         }
         .elementor-element .elementor-widget-container{-webkit-transition:background .3s,border .3s,-webkit-border-radius .3s,-webkit-box-shadow .3s;transition:background .3s,border .3s,-webkit-border-radius .3s,-webkit-box-shadow .3s;-o-transition:background .3s,border .3s,border-radius .3s,box-shadow .3s;transition:background .3s,border .3s,border-radius .3s,box-shadow .3s;transition:background .3s,border .3s,border-radius .3s,box-shadow .3s,-webkit-border-radius .3s,-webkit-box-shadow .3s;}
         }

         @media all{
         .elementor-33 .elementor-element.elementor-element-3uh2bpk{background-color:#f7f7f9;background-image:url("/static/images/common/slide.png");background-position:bottom center;background-repeat:no-repeat;background-size:contain;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:90px 0px 100px 0px;}
         .elementor-33 .elementor-element.elementor-element-3uh2bpk > .elementor-background-overlay{background-image:url("/static/images/common/ssd.png");background-position:bottom center;background-repeat:no-repeat;opacity:1;transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}
         .elementor-33 .elementor-element.elementor-element-eb4719c{text-align:center;}
         .elementor-33 .elementor-element.elementor-element-eb4719c .thm-heading-title{color:#000000;font-family:"Montserrat", Sans-serif;font-size:36px;font-weight:300;}
         .elementor-33 .elementor-element.elementor-element-eb4719c .sub-title-content{color:#000000;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:300;}
         .elementor-33 .elementor-element.elementor-element-eb4719c > .elementor-widget-container{margin:0px 0px 35px 0px;}
         @media (max-width:1024px){
         .elementor-33 .elementor-element.elementor-element-eb4719c .thm-heading-title{font-size:25px;}
         .elementor-33 .elementor-element.elementor-element-eb4719c .sub-title-content{font-size:16px;}
         }
         @media (max-width:767px){
         .elementor-33 .elementor-element.elementor-element-3uh2bpk{padding:50px 0px 50px 0px;}
         .elementor-33 .elementor-element.elementor-element-eb4719c .thm-heading-title{line-height:1.2em;}
         }
         }
         /*! CSS Used fontfaces */
         @font-face{font-family:FontAwesome;src:url(/static/fonts/fontawesome-webfont.eot?v=4.4.0);src:url(/static/fonts/fontawesome-webfont.eot#iefix&v=4.4.0) format('embedded-opentype'),url(/static/fonts/fontawesome-webfont.woff2?v=4.4.0) format('woff2'),url(/static/fonts/fontawesome-webfont.woff?v=4.4.0) format('woff'),url(/static/fonts/fontawesome-webfont.ttf?v=4.4.0) format('truetype'),url(/static/fonts/fontawesome-webfont.svg?v=4.4.0#fontawesomeregular) format('svg');}
         @font-face{font-family:'backnow';src:url('/static/fonts/backnow.eot?ise4yk');src:url('/static/fonts/backnow.eot?ise4yk#iefix') format('embedded-opentype'),     url('/static/fonts/backnow.ttf?ise4yk') format('truetype'),     url('/static/fonts/backnow.woff?ise4yk') format('woff'),     url('/static/fonts/backnow.svg?ise4yk#backnow') format('svg');font-weight:normal;font-style:normal;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:100;src:local('Montserrat Thin'), local('Montserrat-Thin'), url(https://fonts.gstatic.com/s/montserrat/v12/JTUQjIg1_i6t8kCHKm45_QpRxC7mw9c.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:100;src:local('Montserrat Thin'), local('Montserrat-Thin'), url(https://fonts.gstatic.com/s/montserrat/v12/JTUQjIg1_i6t8kCHKm45_QpRzS7mw9c.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:100;src:local('Montserrat Thin'), local('Montserrat-Thin'), url(https://fonts.gstatic.com/s/montserrat/v12/JTUQjIg1_i6t8kCHKm45_QpRxi7mw9c.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:100;src:local('Montserrat Thin'), local('Montserrat-Thin'), url(https://fonts.gstatic.com/s/montserrat/v12/JTUQjIg1_i6t8kCHKm45_QpRxy7mw9c.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:100;src:local('Montserrat Thin'), local('Montserrat-Thin'), url(https://fonts.gstatic.com/s/montserrat/v12/JTUQjIg1_i6t8kCHKm45_QpRyS7m.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:200;src:local('Montserrat ExtraLight'), local('Montserrat-ExtraLight'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_aZA3gTD_u50.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:200;src:local('Montserrat ExtraLight'), local('Montserrat-ExtraLight'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_aZA3g3D_u50.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:200;src:local('Montserrat ExtraLight'), local('Montserrat-ExtraLight'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_aZA3gbD_u50.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:200;src:local('Montserrat ExtraLight'), local('Montserrat-ExtraLight'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_aZA3gfD_u50.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:200;src:local('Montserrat ExtraLight'), local('Montserrat-ExtraLight'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_aZA3gnD_g.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:300;src:local('Montserrat Light'), local('Montserrat-Light'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_cJD3gTD_u50.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:300;src:local('Montserrat Light'), local('Montserrat-Light'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_cJD3g3D_u50.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:300;src:local('Montserrat Light'), local('Montserrat-Light'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_cJD3gbD_u50.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:300;src:local('Montserrat Light'), local('Montserrat-Light'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_cJD3gfD_u50.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:300;src:local('Montserrat Light'), local('Montserrat-Light'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_cJD3gnD_g.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:400;src:local('Montserrat Regular'), local('Montserrat-Regular'), url(https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459WRhyzbi.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:400;src:local('Montserrat Regular'), local('Montserrat-Regular'), url(https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459W1hyzbi.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:400;src:local('Montserrat Regular'), local('Montserrat-Regular'), url(https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459WZhyzbi.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:400;src:local('Montserrat Regular'), local('Montserrat-Regular'), url(https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459Wdhyzbi.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:400;src:local('Montserrat Regular'), local('Montserrat-Regular'), url(https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459Wlhyw.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:500;src:local('Montserrat Medium'), local('Montserrat-Medium'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_ZpC3gTD_u50.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:500;src:local('Montserrat Medium'), local('Montserrat-Medium'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_ZpC3g3D_u50.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:500;src:local('Montserrat Medium'), local('Montserrat-Medium'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_ZpC3gbD_u50.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:500;src:local('Montserrat Medium'), local('Montserrat-Medium'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_ZpC3gfD_u50.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:500;src:local('Montserrat Medium'), local('Montserrat-Medium'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_ZpC3gnD_g.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:600;src:local('Montserrat SemiBold'), local('Montserrat-SemiBold'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_bZF3gTD_u50.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:600;src:local('Montserrat SemiBold'), local('Montserrat-SemiBold'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_bZF3g3D_u50.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:600;src:local('Montserrat SemiBold'), local('Montserrat-SemiBold'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_bZF3gbD_u50.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:600;src:local('Montserrat SemiBold'), local('Montserrat-SemiBold'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_bZF3gfD_u50.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:600;src:local('Montserrat SemiBold'), local('Montserrat-SemiBold'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_bZF3gnD_g.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:700;src:local('Montserrat Bold'), local('Montserrat-Bold'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3gTD_u50.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:700;src:local('Montserrat Bold'), local('Montserrat-Bold'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3g3D_u50.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:700;src:local('Montserrat Bold'), local('Montserrat-Bold'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3gbD_u50.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:700;src:local('Montserrat Bold'), local('Montserrat-Bold'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3gfD_u50.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
         @font-face{font-family:'Montserrat';font-style:normal;font-weight:700;src:local('Montserrat Bold'), local('Montserrat-Bold'), url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3gnD_g.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
      </style>
               <style type="text/css">

            @media all{
            #section-4 .fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
            #section-4 .fa-minus-circle:before{content:"\f056";}
            #section-4 .fa-plus:before{content:"\f067";}
            #section-4 .fa-mail-reply:before{content:"\f112";}
            }

            @media all{
            div,span,h2,h3,p,a,img,strong,i{margin:0;padding:0;border:0;outline:0;vertical-align:bottom;background:transparent;}
            #section-4 a{vertical-align:baseline;}
            body{line-height:1;}
            :focus{outline:0;}
            body{display:block;height:100%;min-height:100%;}
            h2,h3,span,p{margin:0;padding:0;}
            #section-4 img{border:none;}
            }

            @media all{
            [class*=" po-"]:before{font-family:portus;font-style:normal;font-weight:400;speak:none;display:inline-block;text-decoration:inherit;width:1em;margin-right:.2em;text-align:center;font-variant:normal;text-transform:none;line-height:1em;margin-left:.2em;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
            #section-4 .po-portus:before{content:'\e801';}
            #section-4 .po-clock:before{content:'\e802';}
            }

            @media all{
            body{margin:0;}
            #section-4 aside{display:block;}
            #section-4 a{background-color:transparent;}
            #section-4 a:active,a:hover{outline:0;}
            strong{font-weight:700;}
            #section-4 img{border:0;}
            @media print{
            *,:after,:before{color:#000!important;text-shadow:none!important;background:0 0!important;-webkit-box-shadow:none!important;box-shadow:none!important;}
            #section-4 a,a:visited{text-decoration:underline;}
            #section-4 a[href]:after{content:" (" attr(href) ")";}
            #section-4 a[href^="#"]:after{content:"";}
            #section-4 img{page-break-inside:avoid;}
            #section-4 img{max-width:100%!important;}
            h2,h3,p{orphans:3;widows:3;}
            h2,h3{page-break-after:avoid;}
            }
            *{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
            :after,:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
            body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff;}
            #section-4 a{color:#337ab7;text-decoration:none;}
            #section-4 a:focus,a:hover{color:#23527c;text-decoration:underline;}
            #section-4 a:focus{outline:thin dotted;outline:5px auto -webkit-focus-ring-color;outline-offset:-2px;}
            #section-4 img{vertical-align:middle;}
            h2,h3{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}
            h2,h3{margin-top:20px;margin-bottom:10px;}
            h2{font-size:30px;}
            h3{font-size:24px;}
            p{margin:0 0 10px;}
            }

            @media all{
            body{font-size:16px;font-family:"Montserrat",Arial,sans-serif;margin:0;padding:0;background:#fff;}
            body,a{color:#5e5e5e;}
            #section-4 a{text-decoration:none;transition:all .2s;-moz-transition:all .2s;-webkit-transition:all .2s;-o-transition:all .2s;}
            #section-4 a:hover{color:#256dc1;text-decoration:none;}
            #section-4 a:hover,a:focus{outline:0;text-decoration:none;}
            #section-4 a:active{outline:0;text-decoration:none;}
            #section-4 .wrapper{width:1200px;margin:0 auto;position:relative;color:inherit;}
            #section-4 .boxed{background:#f6f6f6;}
            #section-4 img{max-width:100%;height:auto;}
            h3 a{color:#313131;}
            h2,h3{letter-spacing:-.4px;color:#313131;font-weight:900;margin-top:0;}
            p{font-family:"Open Sans",Arial,sans-serif;margin-bottom:30px;}
            #content{display:block;padding-bottom:30px;}
            #section-4 .paragraph-row.portus-main-content-panel,.portus-main-content-panel{display:block;margin-top:30px;margin-bottom:30px;}
            #section-4 .paragraph-row.portus-main-content-panel:last-child{margin-bottom:0;}
            #section-4 .portus-main-content-s-block{display:block;}
            #section-4 .portus-main-content-s-block:after{display:block;content:'';clear:both;}
            #section-4 .portus-main-content-s-block .portus-main-content{display:block;float:left;width:55%;margin-left:3.333333333333333%;}
            #section-4 .portus-main-content-s-block .portus-main-content:first-child{margin-left:0;}
            #section-4 .portus-main-content-s-block .portus-main-content.portus-main-content-s-4{width:71.66666666666667%;}
            #section-4 .portus-content-block{margin-bottom:30px;}
            #section-4 .portus-content-block:last-child{margin-bottom:0;}
            #section-4 .portus-content-block .portus-content-title,.portus-main-content-panel .portus-content-title{display:block;margin-bottom:20px;border-bottom:2px solid #e4e3e2;position:relative;background-color:#ececec;padding:12px 15px 4px;}
            #section-4 .portus-content-block .portus-content-title h2,.portus-main-content-panel .portus-content-title h2{margin-top:0;font-size:22px;}
            #section-4 .article-blog-default .item-content .item-meta .item-stars{margin-bottom:0;margin-left:5px;}
            #section-4 .item-header.item-header-hover{display:block;position:relative;}
            #section-4 .item-header.item-header-hover img{width:100%;}
            #section-4 .item-header.item-header-hover>a:before{display:block;position:absolute;left:0;top:0;width:100%;height:100%;content:'';background:#256dc1;z-index:0;opacity:0;filter:alpha(opacity=0);transition:all .2s;-moz-transition:all .2s;-webkit-transition:all .2s;-o-transition:all .2s;}
            #section-4 .item-header.item-header-hover:hover>a:before{opacity:.8;filter:alpha(opacity=80);}
            #section-4 .item-header.item-header-hover .item-header-hover-buttons{display:inline-block;position:absolute;z-index:2;text-align:center;font-size:0;line-height:100%;vertical-align:middle;opacity:0;top:50%;left:50%;filter:alpha(opacity=0);-webkit-transform:translate(-50%,-50%) scale(.4);-ms-transform:translate(-50%,-50%) scale(.4);transform:translate(-50%,-50%) scale(.4);transition:all .2s;-moz-transition:all .2s;-webkit-transition:all .2s;-o-transition:all .2s;}
            #section-4 .item-header.item-header-hover:hover .item-header-hover-buttons{opacity:1;filter:alpha(opacity=100);-webkit-transform:translate(-50%,-50%) scale(1);-ms-transform:translate(-50%,-50%) scale(1);transform:translate(-50%,-50%) scale(1);}
            #section-4 .item-header.item-header-hover .item-header-hover-buttons a{display:inline-block;font-size:16px;border-radius:50%;background-color:transparent;width:40px;height:40px;line-height:40px;margin:0 5px;border:1px solid #fff;color:#fff;position:relative;}
            #section-4 .item-header.item-header-hover .item-header-hover-buttons span:hover a{background-color:#fff;color:#337ab7;}
            #section-4 .item-header.item-header-hover .item-header-hover-buttons a:after{padding:5px;display:block;position:absolute;left:0;top:0;width:100%;height:100%;content:'';margin:-6px -5px -5px -6px;border:1px solid transparent;box-sizing:content-box;}
            #section-4 .item-header.item-header-hover .item-header-hover-buttons span:after{display:none;position:absolute;left:50%;bottom:60px;content:attr(data-hover-text-me);font-size:15px;font-weight:700;color:#fff;white-space:nowrap;opacity:0;filter:alpha(opacity=0);-webkit-transform:translate(-50%,-50%) scale(1);-ms-transform:translate(-50%,-50%) scale(1);transform:translate(-50%,-50%) scale(1);}
            #section-4 .item-header.item-header-hover .item-header-hover-buttons span:hover:after{display:block;-webkit-animation:animateintext .2s;-moz-animation:animateintext .2s;animation:animateintext .2s;opacity:1;filter:alpha(opacity=100);}
            #section-4 .widget .widget-view-more{clear:both;display:block;padding:13px;font-size:14px;font-weight:700;background-color:#efefef;letter-spacing:-.4px;text-align:center;margin-top:20px;color:#919191;border-radius:3px;border-bottom:1px solid #dcdcdc;text-shadow:0 1px 0 rgba(255,255,255,.5);box-shadow:inset 0 120px 100px -100px rgba(255,255,255,.2);}
            #section-4 .widget .widget-view-more:hover{background-color:#eaeaea;}
            #section-4 .widget .widget-view-more:active{background-color:#eaeaea;box-shadow:inset 0 120px 100px -100px rgba(0,0,0,.08);}
            #section-4 .portus-main-content>div>.paragraph-row{margin-bottom:30px;}
            #section-4 .portus-main-content>div>.paragraph-row:last-child{margin-bottom:0;}
            #section-4 .portus-content-block{display:block;clear:both;}
            #section-4 .article-blog-default{display:block;}
            #section-4 .article-blog-default .item{display:block;padding:25px;border-bottom:1px solid #e4e3e2;margin-bottom:25px;background: white;}
            #section-4 .article-blog-default .item:last-child{margin-bottom:0;padding-bottom:0;border-bottom:0;}
            #section-4 .article-blog-default .item:after{display:block;clear:both;content:'';}
            #section-4 .article-blog-default .item-header{float:left;display:block;width:38%;}
            #section-4 .article-blog-default .item-content{display:block;margin-left:41%;}
            #section-4 .article-blog-default .item-content h3{font-size:20px;letter-spacing:0;margin-bottom:15px;}
            #section-4 .article-blog-default .item-content .item-meta{display:block;font-size:14px;color:#919191;font-weight:700;margin-bottom:15px;}
            #section-4 .article-blog-default .item-content .item-meta .item-meta-i{color:inherit;display:inline-block;margin-right:8px;}
            #section-4 .article-blog-default .item-content .item-meta .item-meta-i i{padding-right:4px;}
            #section-4 .article-blog-default .item-content .item-meta a.item-meta-i:hover{color:#232323;}
            #section-4 .article-blog-default .item-content p{display:block;margin-bottom:0;font-size:15px;}
            #section-4 .sidebar{display:block;float:left;margin-left:3.333333333333333%;}
            #section-4 .sidebar.portus-sidebar-large{width:25%;font-size:14px;color:#b5b5b5;}
            #section-4 .sidebar .widget{display:block;padding-bottom:30px;margin-bottom:20px;border-bottom:1px dotted #e0e0e0;}
            #section-4 .sidebar .widget:last-child{padding-bottom:0;margin-bottom:0;border-bottom:0;}
            #section-4 .widget .tagcloud{display:block;cursor:default;}
            #section-4 .widget .tagcloud a{float:left;font-size:12px!important;color:#919191;padding:7px 10px;display:inline-block;background-color:#efefef;margin-right:5px;margin-bottom:6px;border-radius:3px;border-bottom:1px solid #dcdcdc;text-shadow:0 1px 0 rgba(255,255,255,.5);box-shadow:inset 0 120px 100px -100px rgba(255,255,255,.2);}
            #section-4 .widget .tagcloud a:hover{color:#fff;background-color:#256dc1;text-shadow:0 1px 0 rgba(0,0,0,.2);}
            #section-4 .item-stars{display:inline-block;font-family:"FontAwesome";position:relative;letter-spacing:2px;margin-right:5px;margin-bottom:4px;}
            #section-4 .item-stars:before{display:block;content:'\f005\f005\f005\f005\f005';color:#d1d1d1;}
            #section-4 .item-stars .stars-inner{width:100%;overflow:hidden;position:absolute;left:0;top:0;display:block;z-index:2;}
            #section-4 .item-stars .stars-inner:before{display:block;content:'\f005\f005\f005\f005\f005';color:#fd9d20;}
            #section-4 .w-flickr-feed{display:block;}
            #section-4 .w-flickr-feed .item{display:block;float:left;width:31%;margin-left:3.5%;}
            #section-4 .w-flickr-feed .item:nth-child(3n+1){clear:both;margin-left:0;}
            #section-4 .w-flickr-feed .item:nth-child(n+4){margin-top:10px;}
            #section-4 .w-flickr-feed:after{display:block;clear:both;content:'';}
            #section-4 .item-stars{font-size:12px;}
            #section-4 .paragraph-row{display:block;clear:both;position:relative;margin-bottom:0;}
            #section-4 .paragraph-row:after{display:block;clear:both;content:'';}
            #section-4 .paragraph-row .column12{width:100%;}
            #section-4 .paragraph-row>div{float:left;margin-left:2.127659574468085%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
            #section-4 .paragraph-row>div:first-child{margin-left:0;}
            body.ot-clean{font-family:"Open Sans";}
            body.ot-clean .article-blog-default .item-content h3{font-size:26px;line-height:135%;font-weight:600;}
            body.ot-clean .article-blog-default .item-content .item-meta{display:block;font-size:14px;margin-bottom:13px;color:#b5b5b5;font-weight:500;}
            body.ot-clean .portus-content-block .portus-content-title h2,body.ot-clean .portus-main-content-panel .portus-content-title h2{font-weight:600;}
            body.ot-clean .article-blog-default .item{background-color:#fff;padding:30px;border:0;}
            body.ot-clean .article-blog-default .item-content{margin-left:43%;}
            #section-4 .widget .tagcloud a:hover,.item-header.item-header-hover>a:before{background-color:#5e8dcf;}
            #section-4 a:hover,.item-header.item-header-hover .item-header-hover-buttons span:hover a{color:#5e8dcf;}
            body.ot-clean h2,body.ot-clean h3{letter-spacing:0;font-family:"Lato";color:#101010;}
            #section-4 .portus-content-block .portus-content-title h2,.portus-main-content-panel .portus-content-title h2{display:inline-block;border-bottom:2px solid #5e8dcf;padding-bottom:10px;margin-bottom:-6px;}
            #section-4 .portus-sidebar-large .widget>.widget-title{display:block;width:100%;border-bottom:2px solid rgba(0,0,0,.1);padding-bottom:0;margin-bottom:21px;}
            #section-4 .portus-sidebar-large .widget>.widget-title>h3{display:inline-block;font-size:16px;font-weight:600;border-bottom:2px solid #5e8dcf;padding-bottom:17px;margin-bottom:-2px;}
            #section-4 .post h3{margin-bottom:20px;}
            #section-4 .post:after{display:block;clear:both;content:'';}
            }

            @media all{
            body #section-4,#dat-menu{width:100%;}
            #section-4 .dat-menu-wrapper.dat-menu-padding{padding-top:0;}
            }

            @media all{
            @media only screen and (max-width:1200px){
            #section-4 .boxed{max-width:100%;}
            #section-4 .wrapper{width:97%;overflow:hidden;}
            }
            @media only screen and (max-width:900px){
            #section-4 a,body,div,p{-webkit-text-size-adjust:none;letter-spacing:.015em;}
            #section-4 body.ot-clean .article-blog-default .item-content h3{font-size:18px;}
            }
            @media only screen and (max-width:768px){
            #section-4 .portus-main-content-s-block>.paragraph-row>aside,.portus-main-content-s-block>.paragraph-row>div,.portus-main-content-s-block>div{float:none;clear:both;width:100%!important;max-width:100%;margin-left:0!important;margin-right:0!important;margin-bottom:20px;}
            #section-4 body.ot-clean .article-blog-default .item-content{margin-left:0;}
            #section-4 .wrapper{width:94%;}
            #section-4 body,p{font-size:14px;}
            #section-4 h2{font-size:20px;}
            }
            @media only screen and (max-width:600px){
            #section-4 .paragraph-row>div{float:none;width:100%!important;max-width:100%;margin-left:0;display:block;}
            #section-4 .portus-content-block .portus-content-title h2,.portus-main-content-panel .portus-content-title h2{font-size:17px;}
            #section-4 .article-blog-default .item-content,.article-blog-default .item-header{float:none;width:100%!important;max-width:100%;margin-left:0;display:block;margin-bottom:20px;}
            }
            }

            @media all{
            #section-4 .widget .tagcloud a:hover,.item-header.item-header-hover>a:before{background-color:#5E8DCF;}
            #section-4 a:hover,.item-header.item-header-hover .item-header-hover-buttons span:hover a{color:#5E8DCF;}
            #section-4 h2,h3{font-family:"Lato";}
            #section-4 body,p{font-family:"Open Sans";}
            }

            @media all{
            #section-4 img{height:auto!important;}
            #section-4 .ot-display-none{display:none;}
            }

            #section-4 .theiaStickySidebar:after{content:"";display:table;clear:both;}

            #section-4 .theiaStickySidebar:after{content:"";display:table;clear:both;}
            /*! CSS Used keyframes */
            @-webkit-keyframes animateintext{0%{bottom:90px;opacity:0;filter:alpha(opacity=0);}100%{bottom:60px;opacity:1;filter:alpha(opacity=100);}}
            @-moz-keyframes animateintext{0%{bottom:90px;opacity:0;filter:alpha(opacity=0);}100%{bottom:60px;opacity:1;filter:alpha(opacity=100);}}
            @keyframes animateintext{0%{bottom:90px;opacity:0;filter:alpha(opacity=0);}100%{bottom:60px;opacity:1;filter:alpha(opacity=100);}}
            /*! CSS Used fontfaces */
            @font-face{font-family:FontAwesome;src:url(/static/fonts/fontawesome-webfont.eot?v=4.4.0);src:url(/static/fonts/fontawesome-webfont.eot#iefix&v=4.4.0) format('embedded-opentype'),url(/static/fonts/fontawesome-webfont.woff2?v=4.4.0) format('woff2'),url(/static/fonts/fontawesome-webfont.woff?v=4.4.0) format('woff'),url(/static/fonts/fontawesome-webfont.ttf?v=4.4.0) format('truetype'),url(/static/fonts/fontawesome-webfont.svg?v=4.4.0#fontawesomeregular) format('svg');}
            @font-face{font-family:portus;src:url(/static/fonts/portus.eot?7069820);src:url(/static/fonts/portus.eot?7069820#iefix) format('embedded-opentype'),url(/static/fonts/portus.woff?7069820) format('woff'),url(/static/fonts/portus.ttf?7069820) format('truetype'),url(/static/fonts/portus.svg?7069820#portus) format('svg');}
         </style>
      <style>

         #section-2 .la{display:inline-block;font:normal normal normal 16px/1 LineAwesome;font-size:inherit;text-decoration:inherit;text-rendering:optimizeLegibility;text-transform:none;-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;font-smoothing:antialiased;}
         #section-2 .la-building-o:before{content:"\f155";}
         #section-2 .la-certificate:before{content:"\f178";}
         #section-2 .la-desktop:before{content:"\f1b7";}
         #section-2 .la-gears:before{content:"\f20d";}
         #section-2 .la-globe:before{content:"\f219";}
         #section-2 .la-line-chart:before{content:"\f267";}
         #section-2 .la-money:before{content:"\f294";}
         #section-2 .la-users:before{content:"\f369";}
         #section-2 *,*::before,*::after{box-sizing:inherit;}
         #section-2 *,*::before,*::after{box-sizing:border-box;}
         #section-2 main,section{display:block;}
         #section-2 body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff;}
         #section-2 [tabindex="-1"]:focus{outline:0!important;}
         #section-2 h3,h4{margin-top:0;margin-bottom:.5rem;}
         #section-2 ul{margin-top:0;margin-bottom:1rem;}
         #section-2 a{color:#007bff;text-decoration:none;background-color:transparent;-webkit-text-decoration-skip:objects;}
         #section-2 a:hover{color:#0056b3;text-decoration:underline;}
         #section-2 h3,h4{margin-bottom:.5rem;font-family:inherit;font-weight:500;line-height:1.2;color:inherit;}
         #section-2 h3{font-size:1.75rem;}
         #section-2 h4{font-size:1.5rem;}
         @media print{
         #section-2 *,*::before,*::after{text-shadow:none!important;box-shadow:none!important;}
         #section-2 a:not(.btn){text-decoration:underline;}
         #section-2 h3{orphans:3;widows:3;}
         #section-2 h3{page-break-after:avoid;}
         #section-2 body{min-width:992px!important;}
         #section-2 .container{min-width:992px!important;}
         }
         #section-2 *{box-sizing:border-box;}
         #section-2 body{font-family:"Varela Round","Montserrat","Quicksand",sans-serif;font-size:14px;line-height:1.75;color:#576366;-ms-word-wrap:break-word;word-wrap:break-word;overflow-x:hidden;-webkit-font-smoothing:antialiased;}
         #section-2 a{text-decoration:none;color:#384047;}
         #section-2 a:hover,a:focus{text-decoration:none;}
         #section-2 .section-header{text-align:center;margin-bottom:50px;}
         #section-2 .section-header .section-title{font-size:30px;margin:.2em 0;line-height:1.5;color:#202020;}
         @media (max-width:767.98px){
         #section-2 .section-header .section-title{font-size:26px;line-height:1.2;}
         }
         #section-2 .section-header .section-sub-title{font-size:16px;color:#888;}
         @media (max-width:767.98px){
         #section-2 .section-header .section-sub-title{font-size:14px;}
         }
         #section-2 .jh-section{margin-bottom:70px;}
         #section-2 .container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;}
         @media (min-width:576px){
         #section-2 .container{max-width:540px;}
         }
         @media (min-width:768px){
         #section-2 .container{max-width:720px;}
         }
         @media (min-width:992px){
         #section-2 .container{max-width:960px;}
         }
         @media (min-width:1200px){
         #section-2 .container{max-width:1140px;}
         }
         @media (min-width:1200px){
         #section-2 .container{max-width:1170px;}
         }
         #section-2 .site-content-inner{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px;}
         #section-2 .site-content-inner>.content-area,.site-content-inner>.woocommerce{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px;-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%;}
         #section-2 h3,h4{font-family:"Quicksand",sans-serif;font-weight:600;text-rendering:optimizeLegibility;color:#384047;}
         #section-2 h3{font-size:20px;}
         #section-2 h4{font-size:18px;}
         #section-2 .page-template-template-homepage-v1 .site-content{margin-bottom:0;}
         #section-2 .off-canvas-wrapper{width:100%;overflow-x:hidden;position:relative;-webkit-backface-visibility:hidden;backface-visibility:hidden;-webkit-overflow-scrolling:auto;}
         #section-2 .off-canvas-wrapper #page{overflow:hidden;}
         #section-2 .site-content{margin-bottom:40px;}
         #section-2 h3,h4{margin:.67em 0;}
         #section-2 .job-categories-section .job-categories{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px;}
         #section-2 .job-categories-section .job-category{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px;}
         @media (min-width:768px){
         #section-2 .job-categories-section .job-category{-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%;}
         }
         @media (min-width:992px){
         #section-2 .job-categories-section .job-category{-webkit-box-flex:0;-ms-flex:0 0 33.3333333333%;flex:0 0 33.3333333333%;max-width:33.3333333333%;}
         }
         @media (min-width:1200px){
         #section-2 .job-categories-section .job-category{-webkit-box-flex:0;-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%;}
         }
         #section-2 .job-categories-section .job-categories{margin-bottom:46px;}
         #section-2 .job-categories-section .job-category a{display:block;}
         #section-2 .job-categories-section .job-category i{font-size:63px;margin-bottom:20px;-webkit-transition:all 0.4s ease 0s;transition:all 0.4s ease 0s;}
         #section-2 .job-categories-section .action{text-align:center;}
         #section-2 .job-categories-section .action .action-link{display:inline-block;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;border:1px solid transparent;padding:14px 30px;font-size:15px;line-height:1.2;border-radius:8px;-webkit-transition:color 0.15s ease-in-out,background-color 0.15s ease-in-out,border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;transition:color 0.15s ease-in-out,background-color 0.15s ease-in-out,border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;border-width:2px;}
         @media screen and (prefers-reduced-motion:reduce){
         #section-2 .job-categories-section .action .action-link{-webkit-transition:none;transition:none;}
         }
         #section-2 {
         }
         #section-2 .job-categories-section .action .action-link:hover,.job-categories-section .action .action-link:focus{text-decoration:none;}
         #section-2 .job-categories-section .action .action-link:focus{outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25);}
         #section-2 .job-categories-section .action .action-link:disabled{opacity:.65;}
         #section-2 .job-categories-section .action .action-link:not(:disabled):not(.disabled){cursor:pointer;}
         #section-2 .v1 .job-category{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;position:relative;}
         @media (max-width:1199.98px){
         #section-2 .v1 .job-category{border-bottom:1px solid #e8ecec;border-top:1px solid #e8ecec;}
         }
         #section-2 .v1 .job-category>a{display:block;text-align:center;z-index:2;-webkit-transition:all 0.4s ease 0s;transition:all 0.4s ease 0s;}
         #section-2 .v1 .job-category>a:before{content:'';position:absolute;top:0;bottom:0;left:0;right:0;z-index:-1;-webkit-transition:all 0.3s ease 0s;transition:all 0.3s ease 0s;}
         #section-2 .v1 .job-category>a:hover:before,.v1 .job-category>a:focus:before{top:-3px;bottom:-3px;left:-3px;right:-3px;background-color:#fff;box-shadow:0 0 50px rgba(32,32,32,.15);border-radius:8px;}
         #section-2 .v1 .job-category .category-titile{font-size:16px;margin-top:0;margin-bottom:6px;}
         #section-2 .v1 .job-category .job-count{font-size:14px;}
         @media (min-width:1200px){
         #section-2 .v1 .job-category:nth-child(n+5){border-top:1px solid #e8ecec;}
         }
         #section-2 .v1 .job-category + .job-category{border-left:1px solid #e8ecec;}
         @media (min-width:1200px){
         #section-2 .v1 .job-category + .job-category:nth-child(4n+1){border-left:none;}
         }
         #section-2 .job-categories-section{text-align:center;}
         @media (max-width:767.98px){
         #section-2 .job-categories-section .job-category{-ms-flex-negative:0;flex-shrink:0;}
         }
         #section-2 .job-categories-section .job-categories{padding-left:0;list-style:none;}
         @media (max-width:1199.98px){
         #section-2 .job-categories-section .job-categories{overflow-y:scroll;-ms-flex-wrap:nowrap;flex-wrap:nowrap;}
         }
         #section-2 .job-categories-section.v1 .job-category{padding:0;height:205px;}
         #section-2 .v1 .job-category .job-count,.v1 .job-category>a:hover i{color:#fb236a;}
         #section-2 a:hover,.v1 .job-category i{color:#8b91dd;}
         #section-2 .job-categories-section .action .action-link{color:#fb236a;background-color:transparent;background-image:none;border-color:#fb236a;}
         #section-2 .job-categories-section .action .action-link:hover{color:#fff;background-color:#fb236a;border-color:#fb236a;}
         #section-2 .job-categories-section .action .action-link:focus{box-shadow:0 0 0 .2rem rgba(251,35,106,.5);}
         #section-2 .job-categories-section .action .action-link:disabled{color:#fb236a;background-color:transparent;}
         #section-2 *{box-sizing:border-box;}
         #section-2 div{display:block;}
         /*! CSS Used fontfaces */
         @font-face{font-family:LineAwesome;src:url(/static/fonts/line-awesome.eot?v=1.1.);src:url(/static/fonts/line-awesome.eot?v=1.1.#iefix) format("embedded-opentype"),url(/static/fonts/line-awesome.woff2?v=1.1.) format("woff2"),url(/static/fonts/line-awesome.woff?v=1.1.) format("woff"),url(/static/fonts/line-awesome.ttf?v=1.1.) format("truetype"),url(/static/fonts/line-awesome.svg?v=1.1.#fa) format("svg");font-weight:400;font-style:normal;}
         @font-face{font-family:LineAwesome;src:url(/static/fonts/line-awesome.svg?v=1.1.#fa) format("svg");}
      </style>
      <style>

         @media all{
         #section-3 .stats-content .skill-count{display:block;font-size:40px;font-weight:bold;line-height:45px;margin-top:10px;text-align:center;}
         #section-3 .stats-content .skill-count::after{content:'';display:block;height:4px;background:#6cd086;max-width:50px;margin:5px auto 20px;display:none;border-radius:30px;}
         #section-3 .stats-content p{font-size:16px;font-style:normal;line-height:24px;margin-bottom:0;opacity:1;text-align:center;}
         #section-3 .stats-block.statistics{position:relative;}
         #section-3 .stats-content.percentage{padding:20px 0px;position:relative;z-index:2;}
         #section-3 .stats-block .stats-img img{position:absolute;opacity:0.04;max-width:100px;left:20%;top:10%;z-index:1;}
         }

         @media all{
         #section-3 body{margin:0;}
         #section-3 article,main{display:block;}
         #section-3 img{border:0;}
         @media print{
         #section-3 *,*:before,*:after{background:transparent!important;color:#000!important;-webkit-box-shadow:none!important;box-shadow:none!important;text-shadow:none!important;}
         #section-3 img{page-break-inside:avoid;}
         #section-3 img{max-width:100%!important;}
         #section-3 p{orphans:3;widows:3;}
         }
         #section-3 *{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
         #section-3 *:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
         #section-3 body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff;}
         #section-3 img{vertical-align:middle;}
         #section-3 p{margin:0 0 10px;}
         #section-3 .container{margin-right:auto;margin-left:auto;padding-left:15px;padding-right:15px;}
         @media (min-width:768px){
         #section-3 .container{width:750px;}
         }
         @media (min-width:992px){
         #section-3 .container{width:970px;}
         }
         @media (min-width:1200px){
         #section-3 .container{width:1170px;}
         }
         #section-3 .row{margin-left:-15px;margin-right:-15px;}
         #section-3 .col-md-12{position:relative;min-height:1px;padding-left:15px;padding-right:15px;}
         @media (min-width:992px){
         #section-3 .col-md-12{float:left;}
         #section-3 .col-md-12{width:100%;}
         }
         #section-3 .container:before,.container:after,.row:before,.row:after{content:" ";display:table;}
         #section-3 .container:after,.row:after{clear:both;}
         }

         @media all{
         @media only screen and (max-width: 479px){
         #section-3 .stats-content p{font-size:14px!important;}
         }
         @media only screen and (min-width: 768px){
         #section-3 .spacing_mobile{display:none!important;}
         }
         @media only screen and (max-width: 767px){
         #section-3 .vc_row-has-fill>.vc_column_container>.vc_column-inner{padding-top:0px!important;}
         #section-3 .stats-block .stats-img img{display:none;}
         }
         @media only screen and (min-width: 768px) and (max-width: 1023px){
         #section-3 .vc_row-has-fill>.vc_column_container>.vc_column-inner{padding-top:0px!important;}
         #section-3 .stats-block .stats-img img{display:none;}
         }
         @media only screen and (min-width: 992px) and (max-width: 1199px){
         #section-3 .stats-block .stats-img img{left:13%!important;}
         }
         }

         @media all{
         #section-3 .vc_row:after,.vc_row:before{content:" ";display:table;}
         #section-3 .vc_row:after{clear:both;}
         #section-3 .vc_column_container{width:100%;}
         #section-3 .vc_row{margin-left:-15px;margin-right:-15px;}
         #section-3 .vc_col-sm-2,.vc_col-xs-6{position:relative;min-height:1px;padding-left:15px;padding-right:15px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
         #section-3 .vc_col-xs-6{float:left;}
         #section-3 .vc_col-xs-6{width:50%;}
         @media (min-width:768px){
         #section-3 .vc_col-sm-2{float:left;}
         #section-3 .vc_col-sm-2{width:24.66666667%;}
         }
         #section-3 .vc_row[data-vc-full-width]{-webkit-transition:opacity .5s ease;-o-transition:opacity .5s ease;transition:opacity .5s ease;overflow:hidden;}
         #section-3 .vc_column-inner::after,.vc_column-inner::before{content:" ";display:table;}
         #section-3 .vc_column-inner::after{clear:both;}
         #section-3  .vc_column_container{padding-left:0;padding-right:0;}
         #section-3 .vc_column_container>.vc_column-inner{box-sizing:border-box;padding-left:15px;padding-right:15px;width:100%;}
         #section-3 .vc_row-has-fill>.vc_column_container>.vc_column-inner{padding-top:35px;}
         }
      </style>
      <style type="text/css">

         #section-5 .container.df-bg-content{background-color:#fff;}
         #section-5 #df-content-wrapper.df-content-full{background:#fff;}
         #section-5 .df-wraper h4{font-family:Oswald;font-weight:400;font-style:normal;text-transform:none;font-size:28px;line-height:34px;letter-spacing:0;}
         #section-5 body{font-family:Lato;font-weight:400;font-style:normal;text-transform:none;font-size:13px;line-height:20px;letter-spacing:1px;}
         #section-5 .df-category a{font-family:Lato;font-weight:400;font-style:normal;text-transform:uppercase;font-size:10px;line-height:12px;letter-spacing:1px;}
         #section-5 .post-meta a,.post-meta i,.post-meta,.post-meta .post-meta-desc{font-family:Lato;font-weight:400;font-style:normal;text-transform:uppercase;font-size:10px;line-height:14px;letter-spacing:1px;}
         @media (max-width:48em){
         #section-5 .df-wraper h4{font-size:20px;line-height:25px;}
         }
         @media (max-width:34em){
         #section-5 .df-wraper h4{font-size:20px;line-height:25px;}
         }
         #df-content-wrapper .entry-content li a,#df-content-wrapper a{color:#a2a2a2;}
         #section-5 .df-wraper #page h4,.df-wraper #page h4>a{color:#222;}
         #df-content-wrapper li:not(.df-btn){color:#666;}
         #df-content-wrapper .post-meta a,.post-meta a,.post-meta .post-meta-desc a{color:#a2a2a2;}
         #section-5 .df-category a.cat-Galleries{background-color:#917a56;}
         #section-5 .df-category a.cat-Health{background-color:#677e52;}
         #section-5 .df-category a.cat-Music{background-color:#003354;}
         #section-5 .df-category a.cat-On-Trend{background-color:#962d3e;}
         #section-5 .df-category a.cat-Places{background-color:#105b63;}
         #section-5 .df-category a.cat-Review{background-color:#bd4932;}
         #section-5 .df-category a.cat-Steals{background-color:#687061;}
         #section-5  .df-category a.cat-Style{background-color:#1b5244;}
         #section-5 .df-category a.cat-Video{background-color:#374140;}

         @media all{
         #section-5  .ion-ios-arrow-left:before,.ion-ios-arrow-right:before,.ion-share:before{display:inline-block;font-family:"Ionicons";speak:none;font-style:normal;font-weight:normal;font-variant:normal;text-transform:none;text-rendering:auto;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
         #section-5 .ion-ios-arrow-left:before{content:"\f3d2";}
         #section-5   .ion-ios-arrow-right:before{content:"\f3d3";}
         #section-5  .ion-share:before{content:"\f220";}
         }

         @media all{
         .no-padding{padding:0!important;}
         :focus{outline:0;}
         img{max-width:100%;}
         #section-5 .df-wraper a{text-decoration:none;transition:opacity .1s;}
         #df-content-wrapper a:hover{opacity:.8;}
         #df-content-wrapper a.df-img-wrapper{opacity:1;}
         span.overlay{position:absolute;bottom:0;height:100%;width:100%;background:-webkit-linear-gradient(rgba(225,225,225,0) 0,rgba(0,0,0,.5) 100%);background:-o-linear-gradient(rgba(225,225,225,0) 0,rgba(0,0,0,.5) 100%);background:linear-gradient(rgba(225,225,225,0) 0,rgba(0,0,0,.5) 100%);}
         #df-off-canvas-wrap{-o-transition:300ms ease all;-ms-transition:300ms ease all;}
         #section-5 .df-category{list-style:none;margin-top:10px;margin-bottom:10px;font-size:10px;line-height:1em;}
         #section-5 .df-category a{color:#fff;background-color:#222;padding:3px 6px 4px;white-space:nowrap;letter-spacing:1px;text-transform:uppercase;}
         #section-5 li.entry-category{line-height:2;padding:0;}
         li.entry-category a{margin-right:4px;margin-bottom:4px;display:inline-block;}
         #section-5  .post-meta{margin-top:20px;}
         #section-5 .post-meta-desc{display:inline-block;width:100%;}
         #section-5  .entry-category a{color:#fff!important;}
         #df-content-wrapper{overflow:hidden;}
         #section-5   button.df-prev-button{width:50px;height:50px;position:absolute;top:calc(50% - 55.5px);background:rgba(255,255,255,.6);color:#fff;font-size:32px;font-weight:700;opacity:0;transition:all .5s ease;left:10px;}
         #section-5  button.df-prev-button:active,button.df-prev-button:focus,button.df-prev-button:hover{background:rgba(255,255,255,.9);color:#414141;}
         #section-5  button.df-next-button{width:50px;height:50px;position:absolute;top:calc(50% - 55.5px);background:rgba(255,255,255,.6);color:#fff;font-size:32px;font-weight:700;opacity:0;transition:all .5s ease;right:10px;}
         #section-5 button.df-next-button:active,button.df-next-button:focus,button.df-next-button:hover{background:rgba(255,255,255,.9);color:#414141;}
         .slick-slider:hover button.df-next-button,.slick-slider:hover button.df-prev-button{opacity:1;transition:all .5s ease;}
         body #section-5 {margin:0;}
         #section-5  article{display:block;}
         #section-5 a{background-color:transparent;}
         #section-5   a:active,a:hover{outline:0;}
         #section-5  img{border:0;}
         #section-5   button{color:inherit;font:inherit;margin:0;}
         #section-5  button{overflow:visible;}
         #section-5 button{text-transform:none;}
         #section-5 button{-webkit-appearance:button;cursor:pointer;}
         #section-5 button::-moz-focus-inner{border:0;padding:0;}
         #section-5 *,:after,:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
         body #section-5 {color:#333;background-color:#fff;-ms-word-wrap:break-word;word-wrap:break-word;}
         #section-5 button{font-family:inherit;font-size:inherit;line-height:inherit;}
         #section-5  a{text-decoration:none;}
         #section-5  a:focus{outline:dotted thin;outline:-webkit-focus-ring-color auto 5px;outline-offset:-2px;}
         #section-5 img{vertical-align:middle;}
         #section-5.img-responsive{display:block;max-width:100%;height:auto;}
         #section-5 [role=button]{cursor:pointer;}
         #section-5 h4{margin-top:0;margin-bottom:20px;}
         #section-5 ul{margin-top:0;margin-bottom:20px;}
         #section-5  .list-inline{padding-left:0;list-style:none;}
         #section-5 .list-inline>li{display:inline-block;}
         #section-5 .container{margin-right:auto;margin-left:auto;padding-left:18px;padding-right:18px;}
         @media (min-width:768px){
         #section-5   .container{width:756px;}
         }
         @media (min-width:992px){
         #section-5  .container{width:976px;}
         }
         @media (min-width:1200px){
         #section-5  .container{width:1236px;}
         }
         #section-5  .row{margin-left:-18px;margin-right:-18px;}
         #section-5 .col-md-12{position:relative;min-height:1px;padding-left:18px;padding-right:18px;}
         @media (min-width:992px){
         #section-5 .col-md-12{float:left;}
         #section-5 .col-md-12{width:100%;}
         }
         #section-5 .clearfix:after,.clearfix:before,.container:after,.container:before,.row:after,.row:before{content:" ";display:table;}
         #section-5 .clearfix:after,.container:after,.row:after{clear:both;}
         #section-5 .center-block{display:block;margin-left:auto;margin-right:auto;}
         #section-5 .slick-slider{position:relative;display:block;-moz-box-sizing:border-box;box-sizing:border-box;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;-webkit-touch-callout:none;-khtml-user-select:none;-ms-touch-action:pan-y;touch-action:pan-y;-webkit-tap-highlight-color:transparent;}
         #section-5 .slick-list{position:relative;display:block;overflow:hidden;margin:0;padding:0;}
         #section-5 .slick-list:focus{outline:0;}
         #section-5 .slick-slider .slick-list,.slick-slider .slick-track{-webkit-transform:translate3d(0,0,0);-moz-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);-o-transform:translate3d(0,0,0);transform:translate3d(0,0,0);}
         #section-5 .slick-track{position:relative;top:0;left:0;display:block;}
         #section-5 .slick-track:after,.slick-track:before{display:table;content:'';}
         #section-5 .slick-track:after{clear:both;}
         #section-5 .slick-slide{display:none;float:left;height:100%;min-height:1px;}
         #section-5 .slick-slide img{display:block;}
         #section-5 .slick-initialized .slick-slide{display:block;}
         #section-5 .slick-next,.slick-prev{font-size:12px;line-height:0;position:absolute;top:50%;display:block;width:115px;height:51px;padding:0;cursor:pointer;color:#fff;border:none;outline:0;background:0 0;word-spacing:10px;text-transform:uppercase;font-family:Lato;z-index:2;}
         #section-5 .slick-next:focus,.slick-next:hover,.slick-prev:focus,.slick-prev:hover{color:#fff;outline:0;background:0 0;}
         #section-5 .slick-next:focus:before,.slick-next:hover:before,.slick-prev:focus:before,.slick-prev:hover:before{opacity:1;}
         #section-5 .slick-next:before,.slick-prev:before{font-size:20px;line-height:1;opacity:.75;color:#fff;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
         #section-5  .slick-prev{left:0;}
         #section-5 .slick-next{right:0;}
         #section-5 .df-content-full{margin:0;background:#fff;}
         #section-5 img.article-featured-image{margin-bottom:20px;transition-duration:1s;opacity:1;}
         #section-5 img.article-featured-image:first-child{position:relative;opacity:1;}
         #section-5 .vc_row.df-vc-container .layout-1{margin:0;}
         #section-5 .vc_row.df-vc-container .layout-1.full-column .carousel-content{height:378px;}
         #section-5  .vc_row.df-vc-container .layout-1.full-column .carousel-content img{height:107%;width:auto;}
         @media (min-width:375px){
         #section-5   .vc_row.df-vc-container .layout-1.full-column .carousel-content{height:451px;}
         #section-5  .vc_row.df-vc-container .layout-1.full-column .carousel-content img{height:100%;}
         }
         @media (min-width:414px){
         #section-5 .vc_row.df-vc-container .layout-1.full-column .carousel-content{height:503px;}
         }
         @media (min-width:768px){
         #section-5 .vc_row.df-vc-container .layout-1.full-column .carousel-content{height:479px;}
         }
         @media (min-width:1024px){
         #section-5 .vc_row.df-vc-container .layout-1.full-column .carousel-content{height:418px;}
         }
         @media (min-width:1200px){
         #section-5  .vc_row.df-vc-container .layout-1.full-column .carousel-content{height:399px;}
         }
         #section-5 .vc_row.df-vc-container.is-stretch .layout-1 .carousel-content,.vc_row.df-vc-container.is-stretch.no--padding .layout-1 .carousel-content{height:64.7vh;}
         @media (max-width:600px){
         #section-5 .vc_row.df-vc-container.is-stretch .layout-1 .carousel-content,.vc_row.df-vc-container.is-stretch.no--padding .layout-1 .carousel-content{height:378px;}
         }
         #section-5  .vc_row.df-vc-container.is-stretch .layout-1 .carousel-content img,.vc_row.df-vc-container.is-stretch.no--padding .layout-1 .carousel-content img{width:auto;height:100%;}
         @media (max-width:767px){
         #section-5 .vc_row.df-vc-container.is-stretch .layout-1 .carousel-content img,.vc_row.df-vc-container.is-stretch.no--padding .layout-1 .carousel-content img{width:auto;height:100%;}
         }
         #section-5 .df-shortcode-blocks{position:relative;margin:0 -18px;}
         #section-5  .df-shortcode-blocks.main-carousel{padding-bottom:60px;}
         #section-5  .df-shortcode-blocks.main-carousel .carousel-content{position:relative;overflow:hidden;margin-left:1px;}
         #section-5 .df-shortcode-blocks.main-carousel .carousel-content .article-featured-image{margin-bottom:0;max-width:none;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);}
         #section-5  .df-shortcode-blocks.main-carousel .carousel-content .carousel-content-inner{position:absolute;right:0;bottom:0;left:0;padding:30px;}
         #section-5  .df-shortcode-blocks.main-carousel .carousel-content .carousel-content-inner a{color:#fff!important;}
         #section-5 .post-meta a.author-name:after{content:' -';}
         @media (min-width:1200px){
         #section-5   .boxed{width:1272px;margin-right:auto;margin-left:auto;padding-right:36px;padding-left:36px;}
         }
         #section-5 .df-allcontent-wrap{overflow:hidden;}
         #df-off-canvas-wrap{position:relative;width:100%;left:0;-webkit-transition:right .3s ease-in-out,left .3s ease-in-out;-moz-transition:right .3s ease-in-out,left .3s ease-in-out;transition:right .3s ease-in-out,left .3s ease-in-out;-webkit-backface-visibility:hidden;backface-visibility:hidden;}
         #section-5 [aria-hidden=true]{display:none;}
         #section-5 [aria-hidden=false]{display:block;}
         }
         /*! CSS Used fontfaces */
         @font-face{font-family:"Ionicons";src:url("/static/fonts/ionicons.eot?v=2.0.0");src:url("/static/fonts/ionicons.eot?v=2.0.0#iefix") format("embedded-opentype"),url("/static/fonts/ionicons.ttf?v=2.0.0") format("truetype"),url("/static/fonts/ionicons.woff?v=2.0.0") format("woff"),url("/static/fonts/ionicons.svg?v=2.0.0#Ionicons") format("svg");font-weight:normal;font-style:normal;}
      </style>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

   </head>