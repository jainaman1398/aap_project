<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
   <jsp:include page="head.jsp"/>
   <body class="home page-template page-template-homepage page-template-homepage-php page page-id-33 woocommerce-js fullwidth-bg elementor-default elementor-page elementor-page-33" data-elementor-device-mode="desktop">
      <div id="page" class="hfeed site fullwidth">
         <header id="masthead" class="site-header header header-solid">
            <div class="site-header-wrap container">
               <div class="row">
                  <div class="col-lg-5 clearfix col-4">
                     <div class="thm-explore float-left">
                        <a href="#"><i class="back-layout"></i> Explore<i class="fa fa-angle-down"></i></a>
                        <ul class="thm-iconic-category">
                           <li data-color="#33d3c0">
                              <a href="/category">
                              <i class="back-polo-shirt"></i>
                              <span>
Education
</span>
                              </a>
                           </li>
                           <li data-color="#00BED6">
                              <a href="/category">
                              <i class="back-scissors"></i>
                              <span>Health
</span>
                              </a>
                           </li>
                           <li data-color="#00a5e4">
                              <a href="/category">
                              <i class="back-random-line"></i>
                              <span>                                    Electricity & Water
</span>
                              </a>
                           </li>
                           <li data-color="#0088df">
                              <a href="/category">
                              <i class="back-video-camera2"></i>
                              <span>                                                                        Transport & Infrastructure
                                                                                                            </span>
                              </a>
                           </li>
                           <li data-color="#6f63c3">
                              <a href="/category">
                              <i class="back-fast-food"></i>
                              <span>Schemes For You
</span>
                              </a>
                           </li>
                           <li data-color="#ffc658">
                              <a href="/category">
                              <i class="back-gamepad2"></i>
                              <span>                                                                                                                                                Art And Culture</span>
                              </a>
                           </li>
                           <li data-color="#ff945c">
                              <a href="/category">
                              <i class="back-hand-mic"></i>
                              <span>
                                                                                                                                                                                    Other Initiatives</span>
                              </a>
                           </li>
                           <li data-color="#f8666e">
                              <a href="/category">
                              <i class="back-music-note-black-symbol"></i>
                              <span>
                                                                                                                                                                                    AAP Organisation
</span>
                              </a>
                           </li>
                        </ul>
                     </div>

                     <!--/#main-menu-->
                  </div>
                  <!--/.col-md-5-->
                  <div class="d-block d-lg-none col-8">
                     <div class="backnow-login-register">
                        <ul>
                           <!-- Start Campaign Section -->
                           <li><a href="/timeline" class="backnow-login backnow-dashboard">AAP Timeline</a></li>
                           <!-- End Campaign -->
                        </ul>
                     </div>
                  </div>
                  <!--/.col-md-7-->
                  <div class=" col-md-6 col-5 col-sm-6 order-lg-1 col-lg-auto">
                     <div class="themeum-navbar-header">
                        <div class="logo-wrapper">
                           <a class="themeum-navbar-brand" href="/blog">
                           <img class="enter-logo img-responsive" style="height:50px ;width:auto" src="https://seeklogo.com/images/A/aap-aam-aadami-party-logo-66F5BEB325-seeklogo.com.png" alt="Logo" title="Logo">
                           </a>
                        </div>
                     </div>
                     <!--/#themeum-navbar-header-->
                  </div>
                  <!--/.col-md-7-->
                  <div class="col-7 col-sm-6 d-lg-none">

                     <div class="backnow-login-register float-right">
                        <div class="backnow-search-wrap" style="font-size:27px">
                           <a href="#" class="backnow-search search-open-icon"><i class="back-magnifying-glass-2"></i></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 d-lg-none ">
                     <div id="mobile-menu" class="">
                        <div class="collapse navbar-collapse">
                           <ul id="menu-main-menu-1" class="nav navbar-nav">
                              <li id="menu-item-27" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children menu-item-27 active">
                                 <a title="Home" href="/">Home</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <!--/.#mobile-menu-->
                  </div>
                  <div class="d-none d-lg-block order-lg-3 col-lg">
                     <div class="backnow-login-register">
                        <div class="backnow-search-wrap">
                           <a href="#" class="backnow-search search-open-icon"><i class="back-magnifying-glass-2"></i></a>
                        </div>
                        <ul>
                           <li><a href="/timeline" class="backnow-login backnow-dashboard">AAP Timeline</a></li>
                        </ul>
                     </div>
                  </div>
                  <!--/.col-md-7-->
               </div>
               <!--/.main-menu-wrap-->
               <div class="thm-fullscreen-search d-flex flex-wrap justify-content-center align-items-center">
                  <div class="search-overlay"></div>
                  <form action="https://aamaadmiparty.org/?" method="get">
                     <input class="main-font" type="text" value="" name="s" placeholder="Search here..." autocomplete="off">
                     <input type="submit" value="submit" class="d-none" id="thm-search-submit">
                     <label for="thm-search-submit"><i class="fa fa-search"></i></label>
                  </form>
               </div>
               <!--/ .main-menu-wrap -->
            </div>
            <!--/.container-->
         </header>
         <!--/.header-->

         <jsp:include page="${INNER_TEMPLATE}"/>

         <footer id="footer">
            <div class="container">
               <div class="footer-copyright">
                  <div class="row">
                     <div class="col-md-6 text-left copy-wrapper">
                        <img class="enter-logo img-responsive" height="80" style="height:80px" src="https://aamaadmiparty.org/wp-content/uploads/2017/07/aap.png" alt="Logo" title="Logo">
                        <span>© 2018 AAP. All Rights Reserved.</span>
                     </div>
                     <!-- end row -->
                     <div class="col-md-6 text-right copy-wrapper">
                        <div class="social-share">
                           <ul>
                              <li><a target="_blank" href="https://www.facebook.com/AamAadmiParty/"><i class="fa fa-facebook"></i></a></li>
                              <li><a target="_blank" href="https://twitter.com/AamAadmiParty"><i class="fa fa-twitter"></i></a></li>
                              <li><a target="_blank" href="https://plus.google.com/102917260213312355919"><i class="fa fa-google-plus"></i></a></li>
                              <li><a target="_blank" href="https://in.linkedin.com/company/aamaadmiparty"><i class="fa fa-linkedin"></i></a></li>
                           </ul>
                        </div>
                     </div>
                     <!-- end row -->
                  </div>
                  <!-- end row -->
               </div>
               <!-- end row -->
            </div>
            <!-- end container -->
         </footer>
         <!-- End footer -->
      </div>
   </body>
</html>