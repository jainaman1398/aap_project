<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row" style="transform: none;">
   <!-- Main Content Start -->
   <div class="main--content col-md-8 pb--60" data-trigger="stickyScroll" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
      <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; left: 73px; top: 0px;">
         <div class="main--content-inner drop--shadow">
            <!-- Content Nav Start -->
            <!-- Content Nav End -->
            <!-- Filter Nav Start -->
            <div class="filter--nav pb--30 clearfix">
               <div class="filter--options float--right">
                  <label>
                     <span class="fs--14 ff--primary fw--500 text-darker">Show By :</span>
                     <select name="membersfilter" class="form-control form-sm" data-trigger="selectmenu" style="display: none;">
                        <option value="last-active" selected="">Last Active</option>
                        <option value="most-members">Most Members</option>
                        <option value="newly-created">Newly Created</option>
                        <option value="alphabetical">Alphabetical</option>
                     </select>
                  </label>
               </div>
            </div>
            <!-- Filter Nav End -->
            <!-- Box Items Start -->
            <div class="box--items">
               <div class="row gutter--15 " style="position: relative; ">

                <c:forEach items="${pushSettings}" var="pushSetting">
                      <div class="col-md-4 col-xs-6 col-xxs-12" style="left: 0px; top: 0px;">
                         <!-- Box Item Start -->
                         <div class="box--item text-center">
                            <a href="group-home.html" class="img" data-overlay="0.1">
                            <img src="img/group-img/01.jpg" alt="">
                            </a>
                            <div class="info" style="height:200px">
                               <div class="icon fs--18 text-lightest bg-primary">
                                  <i class="fa fa-youtube-play"></i>
                               </div>
                               <div class="title">
                                  <h2 class="h6" style="max-height: 50px;overflow: hidden;"><a href="group-home.html">${pushSetting.purpose}</a></h2>
                               </div>
                               <div class="meta">
                                  <p><i class="fa mr--8 fa-clock-o"></i>Active 1 days ago</p>
                                  <p><i class="fa mr--8 fa-user-o"></i>Currently 2500 Subscribes.</p>
                               </div>
                               <div class="desc text-darker">
                                  <p>${pushSetting.subtitle}</p>
                               </div>
                               <div class="action">
                                  <a href="${pushSetting.result_page}" target="_blank">Link<i class="fa ml--10 fa-caret-right"></i></a>
                               </div>
                            </div>
                         </div>
                         <!-- Box Item End -->
                      </div>
                </c:forEach>

               </div>
            </div>
            <!-- Box Items End -->
            <!-- Page Count Start -->
            <div class="page--count pt--30">
               <label class="ff--primary fs--14 fw--500 text-darker">
               <span>Viewing</span>
               <a href="#" class="btn-link"><i class="fa fa-caret-left"></i></a>
               <input type="number" name="page-count" value="01" class="form-control form-sm">
               <a href="#" class="btn-link"><i class="fa fa-caret-right"></i></a>
               <span>of 28</span>
               </label>
            </div>
            <!-- Page Count End -->
         </div>
         <div class="resize-sensor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; z-index: -1; visibility: hidden;">
            <div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
               <div style="position: absolute; left: 0px; top: 0px; transition: 0s; width: 790px; height: 952px;"></div>
            </div>
            <div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
               <div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%"></div>
            </div>
         </div>
      </div>
   </div>
   <!-- Main Content End -->
   <!-- Main Sidebar Start -->
   <div class="main--sidebar col-md-4 pb--60" data-trigger="stickyScroll" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
      <!-- Widget Start -->
      <!-- Widget End -->
      <!-- Widget Start -->
      <!-- Widget End -->
      <!-- Widget Start -->
      <!-- Widget End -->
      <!-- Widget Start -->
      <!-- Widget End -->
      <!-- Widget Start -->
      <!-- Widget End -->
      <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; left: 853px; top: 0px;">
         <div class="widget">
            <h2 class="h4 fw--700 widget--title">Choose a campaign</h2>
            <!-- Buddy Finder Widget Start -->
            <div class="buddy-finder--widget">
               <form action="#">
                  <div class="row" style="padding:10px">
                     <div class="col-xxs-12">
                        <div class="form-group">
                           <label>
                              <span class="text-darker ff--primary fw--500">Tagret Audience</span>
                              <select name="gender" class="form-control form-sm" data-trigger="selectmenu" style="display: none;">
                                 <option value="male">Recent Campaigns</option>
                                 <option value="female">Top Campaigns</option>
                              </select>
                           </label>
                        </div>
                     </div>
                     <div class="col-xxs-12">
                        <div class="form-group">
                           <label>
                              <span class="text-darker ff--primary fw--500">Campaign Name</span>
                              <select name="campaign_name" class="form-control form-sm" data-trigger="selectmenu">
                                 <option value="Political revolution in India has began">Political revolution in India has began</option>
                              </select>
                           </label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label>
                           <span class="text-darker ff--primary fw--500">Filter State</span>
                           <select name="city" class="form-control form-sm" data-trigger="selectmenu" >
                              <option value="All">All</option>
                              <option value="Delhi">Delhi</option>
                              <option value="Haryana">Haryana</option>
                              <option value="Kerla">Kerla</option>
                              <option value="Punjab">Punjab</option>
                           </select>
                        </label>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <button type="button" onclick="sendNotifications()" class="btn btn-primary">Search</button>
                  </div>
                  <br>
                  <br>
            </div>
            </form>
         </div>
         <!-- Buddy Finder Widget End -->
      </div>

   </div>
</div>
<!-- Main Sidebar End -->
