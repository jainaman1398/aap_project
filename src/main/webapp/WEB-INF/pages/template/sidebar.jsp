<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<aside id="sidebar" class="sidebar sidebar-right portus-sidebar-large" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
 <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 30px; left: 1059px;">
    <div class="widget-5 widget widget_tag_cloud">
       <div class="widget-title">
          <h3>Tags</h3>
       </div>
        <div class="tagcloud"><a href="/category" class="tag-cloud-link tag-link-38 tag-link-position-1" style="font-size: 8pt;" aria-label="Adversarium (1 item)">
            Education
        </a>
       </div>
        <div class="tagcloud"><a href="/category" class="tag-cloud-link tag-link-38 tag-link-position-1" style="font-size: 8pt;" aria-label="Adversarium (1 item)">
            Health
        </a>
       </div>
        <div class="tagcloud"><a href="/category" class="tag-cloud-link tag-link-38 tag-link-position-1" style="font-size: 8pt;" aria-label="Adversarium (1 item)">
            Electricity & Water
        </a>
       </div>
        <div class="tagcloud"><a href="/category" class="tag-cloud-link tag-link-38 tag-link-position-1" style="font-size: 8pt;" aria-label="Adversarium (1 item)">
            Transport & Infrastructure
        </a>
       </div>
        <div class="tagcloud"><a href="/category" class="tag-cloud-link tag-link-38 tag-link-position-1" style="font-size: 8pt;" aria-label="Adversarium (1 item)">
            Schemes For You
        </a>
       </div>
        <div class="tagcloud"><a href="/category" class="tag-cloud-link tag-link-38 tag-link-position-1" style="font-size: 8pt;" aria-label="Adversarium (1 item)">
            Art And Culture
        </a>
       </div>
        <div class="tagcloud"><a href="/category" class="tag-cloud-link tag-link-38 tag-link-position-1" style="font-size: 8pt;" aria-label="Adversarium (1 item)">
            Other Initiatives
        </a>
       </div>
        <div class="tagcloud"><a href="/category" class="tag-cloud-link tag-link-38 tag-link-position-1" style="font-size: 8pt;" aria-label="Adversarium (1 item)">
            AAP Organisation
        </a>
       </div>

    </div>
    <div class="widget-6 last widget widget_orange_themes_flickr" style="margin-top:30px">
       <div class="widget-title">
          <h3>Recent News</h3>
       </div>
       <div class="w-flickr-feed">
         <c:forEach items="${POSTS}" var="post">
          <a href="${post.link}" target="_blank" class="item">
            <img width="150" height="150" src="${post.img}" alt="">
          </a>
         </c:forEach>
       </div>
    </div>
    <iframe  style="margin-top:30px" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FAamAadmiParty%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="350" height="410" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
    <div class="element_size_33 column" style="margin-top:30px">
        <a class="twitter-timeline" data-width="100%" data-height="410" href="https://twitter.com/AamAadmiParty">Tweets by AamAadmiParty</a>
        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
    <div class="element_size_33 column"  style="margin-top:30px">
    <a class="twitter-timeline" data-width="350" data-height="410" href="https://twitter.com/ArvindKejriwal">Tweets by Arvind Kejriwal</a>
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
 </div>
</aside>
