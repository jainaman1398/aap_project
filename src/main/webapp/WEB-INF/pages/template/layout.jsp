<%@ page contentType="text/html; charset=utf-8" language="java" %>
<!DOCTYPE html>
<html dir="ltr" lang="en" style="transform: none;">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- ==== Favicon ==== -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- ==== Google Font ==== -->
      <link rel="stylesheet" href="/static/template/SociFly - Multipurpose Social Network HTML5 Template_files/css">
      <!-- ==== Plugins Bundle ==== -->
      <link rel="stylesheet" href="/static/template/SociFly - Multipurpose Social Network HTML5 Template_files/plugins.min.css">
      <!-- ==== Main Stylesheet ==== -->
      <link rel="stylesheet" href="/static/template/SociFly - Multipurpose Social Network HTML5 Template_files/style.css">
      <!-- ==== Responsive Stylesheet ==== -->
      <link rel="stylesheet" href="/static/template/SociFly - Multipurpose Social Network HTML5 Template_files/responsive-style.css">
      <!-- ==== Color Scheme Stylesheet ==== -->
      <link rel="stylesheet" href="/static/template/SociFly - Multipurpose Social Network HTML5 Template_files/color-1.css" id="changeColorScheme">
      <!-- ==== Custom Stylesheet ==== -->
      <link rel="stylesheet" href="/static/template/SociFly - Multipurpose Social Network HTML5 Template_files/custom.css">
      <style>
         #preloader {
         width: 100%;
         position: relative;
         left: 0px;
         top: 0px;
         height: 100%;
         }
      </style>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/json2html/1.2.0/json2html.min.js"></script>
      <!-- ==== HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries ==== -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style id="ccSwitcher">#cColorSwitcher {position: fixed; top: 50%; left: -220px; z-index: 999;} .ccs--body {float: left; width: 220px; padding: 14px 15px 20px; box-shadow: rgba(0, 0, 0, 0.15) 0px 2px 10px 2px;} .ccs--body:before {content: " "; position: absolute; top: 0; left: 0; right: 50px; bottom: 0; background-color: #fff; z-index: 0;} .ccs--body > h6 {position: relative; margin: 0; font-size: 16px; line-height: 24px; z-index: 1;} .ccs--body > ul {margin: 0 -5px; padding: 0; list-style: none; overflow: hidden;} .ccs--body > ul > li {position: relative; float: left; width: 30px; height: 30px; margin: 10px 5px 0; overflow: hidden; cursor: pointer;} .ccs--body > ul > li > span:nth-child(1) {position: absolute; width: 100%; height: 100%;} .ccs--body > ul > li > span:nth-child(2) {position: absolute; top: 0; right: -2px; width: 20px; height: 45px;} .ccs--toggle-btn {float: right; width: 50px; padding: 15px 0px; color: #fff; font-family: Arial, sans-serif; font-size: 14px; font-weight: 100; line-height: 22px; text-align: center; cursor: pointer; box-shadow: rgba(0, 0, 0, 0.15) 0px 2px 10px 2px;}</style>
      <style id="theia-sticky-sidebar-stylesheet-TSS">.theiaStickySidebar:after {content: ""; display: table; clear: both;}</style>
   </head>
   <body style="transform: none;" class="">
      <!-- Wrapper Start -->
      <div class="wrapper" style="transform: none;">
         <!-- Header Section Start -->
         <header class="header--section style--1">
            <!-- Header Topbar Start -->
            <div class="header--topbar bg-black">
               <div class="container">
                  <!-- Header Topbar Links Start -->
                  <!-- Header Topbar Links End -->
                  <!-- Header Topbar Social Start -->
                  <!-- Header Topbar Social End -->
                  <!-- Header Topbar Links Start -->
                  <ul class="header--topbar-links nav ff--primary float--right">
                     <li>
                        <a href="demo/socifly/html/cart.html" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Cart">
                        <i class="fa fa-shopping-basket"></i>
                        <span class="badge">3</span>
                        </a>
                     </li>
                     <li>
                        <a href="demo/socifly/html/blog-sidebar-left.html#" class="btn-link">
                        <i class="fa mr--8 fa-user-o"></i>
                        <span>My Account</span>
                        </a>
                     </li>
                  </ul>
                  <!-- Header Topbar Links End -->
               </div>
            </div>
            <!-- Header Topbar End -->
            <!-- Header Navbar Start -->
         </header>
         <div class="page--header pt--60 pb--60 text-center bg--img" data-overlay="0.85" data-rjs="2" style="background-image: url(&quot;https://cdn2.i-scmp.com/sites/default/files/styles/980x551/public/2014/01/11/india_politics_arvind_kejriwal_del20_40029767.jpg?itok=fawe4WSN&quot;);">
            <div class="container">
               <h1 style="color:white">Send Push Notifications</h1>
            </div>
         </div>
         <!-- Page Header End -->
         <!-- Page Wrapper Start -->
         <section class="page--wrapper pt--80 pb--20" style="transform: none;">
                     <div class="container" style="transform: none;">
                     <jsp:include page="${INNER}"/>
                     </div>
         </section>
      </div>
      <!-- Wrapper End -->
      <!-- Back To Top Button Start -->
      <div id="backToTop">
         <a href="demo/socifly/html/blog-sidebar-left.html#" class="btn"><i class="fa fa-caret-up"></i></a>
      </div>
      <script src="/static/template/SociFly - Multipurpose Social Network HTML5 Template_files/plugins.min.js"></script>
      <script src="/static/template/SociFly - Multipurpose Social Network HTML5 Template_files/main.js"></script>
      <jsp:include page="/WEB-INF/pages/script.jsp"/>
   </body>
</html>