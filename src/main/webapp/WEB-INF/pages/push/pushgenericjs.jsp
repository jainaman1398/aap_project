 // Initialize Firebase
var config = {
   apiKey: "AIzaSyA8ifE8FsvcAb_IbP6ER43qW4g9BQitnNY",
   authDomain: "notify-2a418.firebaseapp.com",
   databaseURL: "https://notify-2a418.firebaseio.com",
   projectId: "notify-2a418",
   storageBucket: "notify-2a418.appspot.com",
   messagingSenderId: "898878946043"
};
firebase.initializeApp(config);

function getParameterByNameFromUrl(name, url) {
   if (!url) url = window.location.href;
   try {
       name = name.replace(/[\[\]]/g, "\\$&");
       var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
           results = regex.exec(url);
       if (!results) return null;
       if (!results[2]) return '';
       return decodeURIComponent(results[2].replace(/\+/g, " "));
   } catch (er) {}
   return "";
}

function createFirebaseId(deviceToken) {
   var data = {};
   data.device_token = deviceToken;
   data.domain_url = window.location.href;
   data.t1 = document.getElementById("first_name").value;
   data.t2 = window.t2;
   data.t3 = window.t3;
   $.ajax({
       type: "POST",
       url: "/notify/register",
       data: JSON.stringify(data),
       contentType: 'application/json',
       success: function(data) {
           console.log(data);
           showResultPage();
       }
   });
}

window.sendToken = false;

function showResultPage(){
    window.location.href=window.result_page;
}

if (!("Notification" in window)) {
   console.log("This browser does not support system notifications");
}
// Let's check whether notification permissions have already been granted
else {
   if (Notification.permission === "granted") {
      showResultPage();
      showShareContentScreen();
   } else {
       console.log();
          if(Notification.permission==="denied"){
             if(!!window.redirectors[window.location.host]){
                window.location.href="https://"+window.redirectors[window.location.host]+window.location.pathname;
             }
             else {
                 showResultPage();
             }
          }
       window.sendToken = true;
   }
}

if ('serviceWorker' in navigator) {
   navigator.serviceWorker.register('/static/js/firebase.js')
       .then(function(registration) {
           console.log('Service worker has been registered with scope: ', registration.scope);
           const messaging = firebase.messaging();
           messaging.useServiceWorker(registration);

           window.registration = registration;
       })
}

window.show = false;

function showShareContentScreen(){
    document.getElementById("show-loader").style.display = "none";
    document.getElementById("share-content").style.display = "none";
}

function showNotify() {
   const messaging = firebase.messaging();
   var registration = window.registration;
   messaging.requestPermission()
       .then(function() {
           token = messaging.getToken();
           return token;
       })
       .then(function(token) {
           if (window.sendToken) {
               createFirebaseId(token);
           }
           showShareContentScreen();
       })
       .catch(function(err) {
           window.show = false;
           showResultPage();
           console.log('Unable to get notification token. ');
           console.log(err);
       })
   messaging.onMessage(function(payload) {
       console.log("In onMessage function: ");
       console.log("onMessage: ", payload);

       const notificationTitle = payload.notification.title;
       const notificationOptions = {
           body: payload.notification.body,
           icon: payload.notification.icon,
       };

       if (!("Notification" in window)) {
           console.log("This browser does not support system notifications");
       }
       // Let's check whether notification permissions have already been granted
       else if (Notification.permission === "granted") {
           // If it's okay let's create a notification
           var notification = new Notification(notificationTitle, notificationOptions);
           notification.onclick = function(event) {
               event.preventDefault(); // prevent the browser from focusing the Notification's tab
               window.open(payload.notification.click_action, '_blank');
               notification.close();
           }
       }
   })
}

function handleNotify() {
   if (window.show) return;
   window.show = true;
   document.getElementById("show-loader").style.display = "block";
   document.getElementById("share-content").style.display = "none";

   setTimeout(function() {
       showNotify()
   }, 2500);
}

var notigyBTN = document.getElementById('notify-btn');
notigyBTN.addEventListener('click', function() {
    handleNotify();
    return false;
});
