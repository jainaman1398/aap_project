package com.clicdirectory.entity;

/**
 * Created by hackme on 12/21/18.
 */
public class PushSetting {

    public String getLoad_path() {
        return load_path;
    }

    public String getResult_page() {
        return result_page;
    }

    public String getResult_page_name() {
        return result_page_name;
    }

    public String getPurpose() {
        return purpose;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getDescription() {
        return description;
    }

    public String getMore_description() {
        return more_description;
    }

    public String getButton_above() {
        return button_above;
    }

    public String getTitle_color() {
        return title_color;
    }

    public String load_path;
    public String result_page;
    public String result_page_name;
    public String purpose;
    public String subtitle;
    public String description;
    public String more_description;
    public String button_above;
    public String title_color;

    public PushSetting() {

    }

    public PushSetting(String load_path, String result_page, String result_page_name, String purpose, String subtitle, String description, String more_description) {
        this.load_path = load_path;
        this.result_page = result_page;
        this.result_page_name = result_page_name;
        this.purpose = purpose;
        this.subtitle = subtitle;
        this.description = description;
        this.more_description = more_description;
    }

}
