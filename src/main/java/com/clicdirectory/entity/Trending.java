package com.clicdirectory.entity;

public class Trending {
    public String getTag() {
        return tag;
    }

    public String getTitle() {
        return title;
    }

    public String getRtitle() {
        return rtitle;
    }

    public String getImg() {
        return img;
    }

    public String getDescription() {
        return description;
    }

    public String getRdescription() {
        return rdescription;
    }

    public String getLink() {
        return link;
    }

    public Trending(){

    }

    public String tag;
    public String title;
    public String rtitle;
    public String img;
    public String description;
    public String rdescription;
    public String link;
}
