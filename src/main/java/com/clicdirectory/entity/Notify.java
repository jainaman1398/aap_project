package com.clicdirectory.entity;

public class Notify {
    public String title;//"The Utilimate Horo"
    public String body; //"What is your horoscope today ?",
    public String icon;// http://cache1.asset-cache.net/xt/619656278.jpg?v=1&g=fs1|0|SKP534|56|278&s=1&b=RkFD"};
    public String click_action;
    public String image;
    public boolean content_available;

    public Notify(String title, String body, String icon, String click_action, String image, boolean content_available) {
        this.title = title;
        this.body = body;
        this.icon = icon;
        this.click_action = click_action;
        this.image = image;
        this.content_available = content_available;
    }

    public Notify(String title, String body, String icon, String clickAction, boolean content_available) {
        this.title = title;
        this.body = body;
        this.icon = icon;
        this.click_action = clickAction;
        this.content_available = content_available;
    }

}