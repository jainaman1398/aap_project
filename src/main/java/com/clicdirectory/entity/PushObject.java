package com.clicdirectory.entity;

import java.util.HashMap;

public class PushObject {
    public String to;
    public HashMap<String, String> data;
    public Notify notification;

    public PushObject(String to, HashMap<String, String> data, Notify notification) {
        this.to = to;
        this.data = data;
        this.notification = notification;
    }
}