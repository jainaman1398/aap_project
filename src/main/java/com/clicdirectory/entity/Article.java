package com.clicdirectory.entity;

public class Article {
    public String title;

    public String getTitle() {
        return title;
    }

    public String getImg() {
        return img;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public String getLink() {
        return link;
    }

    public Article(){}

    public String img;
    public String description;
    public String date;
    public String link;
}
