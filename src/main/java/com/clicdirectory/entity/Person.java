package com.clicdirectory.entity;

public class Person {
    public String getName() {
        return name;
    }

    public String getImg() {
        return img;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }

    public String getTag() {
        return tag;
    }

    public String name;
    public String img;
    public String subtitle;
    public String link;
    public String title;
    public String tag;
    public Person(){}

}
