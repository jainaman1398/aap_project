package com.clicdirectory.entity;

/**
 * Created by hackme on 12/17/18.
 */
public class NotifyToken {

    public String device_token;
    public String t1;
    public String t2;
    public String t3;
    public String domain_url;

    public NotifyToken() {

    }

    public NotifyToken(String device_token, String t1, String t2, String t3) {
        this.device_token = device_token;
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;
    }
}
