package com.clicdirectory.controller;

import com.clicdirectory.database.GenericDB;
import com.clicdirectory.entity.*;
import com.clicdirectory.global.Randomizer;
import com.clicdirectory.global.SendNotifications;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * User: rishabh
 */
@Controller
@RequestMapping("/admin")
public class AdminController extends BaseController {

    private static Logger logger = Logger.getLogger(AdminController.class);

    @RequestMapping(method = RequestMethod.GET, value = "/layout")
    public static String layout( ModelMap modelMap, HttpServletResponse response, HttpServletRequest request) {
        return "/template/layout";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/layout/{name}")
    public static String layout(@PathVariable("name")String layout_name, ModelMap modelMap, HttpServletResponse response, HttpServletRequest request) {
        modelMap.addAttribute("INNER",layout_name+".jsp");

        if("pushnotify".equals(layout_name)){
            ArrayList<PushSetting> pushSettings = GenericPushController.getPushSettings();
            modelMap.addAttribute("pushSettings",pushSettings);
        }

        return "/template/layout";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/notify/send")
    public @ResponseBody
    ResponseMessage<SendNotify> sendNotify(@RequestBody SendNotify sendNotify, ModelMap modelMap, HttpServletResponse response, HttpServletRequest request) {
        SendNotifications.main(sendNotify.campaignName);
        return new ResponseMessage<SendNotify>("In Progress","success",sendNotify);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public @ResponseBody
    ResponseMessage<MemberBase> apply(@RequestBody Member member, ModelMap modelMap, HttpServletResponse response, HttpServletRequest request) {
        MemberBase memberT = new GenericDB<MemberBase>().getRow(com.clicdirectory.tables.Member.MEMBER,MemberBase.class, com.clicdirectory.tables.Member.MEMBER.PASSWORD.eq(member.password).and(com.clicdirectory.tables.Member.MEMBER.EMAIL.eq(member.email)));
      //  boolean t=new GenericDB<String>().updateColumn(com.clicdirectory.tables.Member.MEMBER.FB_TOKEN, "123", com.clicdirectory.tables.Member.MEMBER, com.clicdirectory.tables.Member.MEMBER.PASSWORD.eq("test"));
       // new GenericDB<Member>().addRow(com.clicdirectory.tables.Member.MEMBER,member);
        if(memberT!=null){
           setSession(request,response,memberT);
           return new ResponseMessage<MemberBase>("Login Successful","success",memberT);
        }
        return new ResponseMessage<MemberBase>("Wrong email password combination","failed",memberT);
    }

    private void setSession( HttpServletRequest request,HttpServletResponse response, MemberBase member) {
        Member m =new GenericDB<Member>().getRow(com.clicdirectory.tables.Member.MEMBER,Member.class, com.clicdirectory.tables.Member.MEMBER.PASSWORD.eq(member.password).and(com.clicdirectory.tables.Member.MEMBER.EMAIL.eq(member.email)));
        String  token = Randomizer.getRandomString(12);
        new GenericDB<String>().updateColumn(com.clicdirectory.tables.Member.MEMBER.TOKEN, token, com.clicdirectory.tables.Member.MEMBER, com.clicdirectory.tables.Member.MEMBER.ID.eq(m.id));
        Cookie cookie = new Cookie(ControllerUtils.TOKEN_COOKIE, token);
        cookie.setPath("/");
        cookie.setMaxAge(-1);
        response.addCookie(cookie);
        ControllerUtils.setUserSession(request,m);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/fb_token")
    public @ResponseBody String apply2(@RequestBody Member member, ModelMap modelMap, HttpServletResponse response, HttpServletRequest request) {
        String x = ControllerUtils.getEmailId(request);
        MemberBase memberT = new GenericDB<MemberBase>().getRow(com.clicdirectory.tables.Member.MEMBER,MemberBase.class, com.clicdirectory.tables.Member.MEMBER.EMAIL.eq(x));

        new GenericDB<String>().updateColumn(com.clicdirectory.tables.Member.MEMBER.FB_TOKEN, member.fb_token, com.clicdirectory.tables.Member.MEMBER,
                com.clicdirectory.tables.Member.MEMBER.EMAIL.eq(memberT.email));
        return "done";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/signup")
    public @ResponseBody ResponseMessage<Member> apply1(@RequestBody Member member, ModelMap modelMap, HttpServletResponse response, HttpServletRequest request) {
        MemberBase memberT = new GenericDB<MemberBase>().getRow(com.clicdirectory.tables.Member.MEMBER,MemberBase.class, (com.clicdirectory.tables.Member.MEMBER.EMAIL.eq(member.email)));
        if(memberT==null&&member.email!=null&&member.password!=null&&member.phone!=null){
            member.role="admin";
            Member member1 = new GenericDB<Member>().addRow(com.clicdirectory.tables.Member.MEMBER,member);
            setSession(request,response, member1);
            return new ResponseMessage<Member>("User Created","success",member1);
        }else {
            return new ResponseMessage<Member>("User Already exists !","failure",null);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/welcome")
    public  String apply(ModelMap modelMap, HttpServletResponse response, HttpServletRequest request) {
        String x = ControllerUtils.getEmailId(request);
        MemberBase memberT = new GenericDB<MemberBase>().getRow(com.clicdirectory.tables.Member.MEMBER,MemberBase.class, com.clicdirectory.tables.Member.MEMBER.EMAIL.eq(x));

       if(memberT==null)
           return "error";
       // modelMap.addAttribute("email",memberT.email);
        return "fb_login";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/fb")
    public  String apply4(ModelMap modelMap, HttpServletResponse response, HttpServletRequest request) {
       /* String x = ControllerUtils.getEmailId(request);
        MemberBase memberT = new GenericDB<MemberBase>().getRow(com.clicdirectory.tables.Member.MEMBER,MemberBase.class, com.clicdirectory.tables.Member.MEMBER.EMAIL.eq(x));

        if(memberT==null)
            return "error";
        // modelMap.addAttribute("email",memberT.email);*/
        return "fb_data";
    }


}