package com.clicdirectory.controller;

import com.clicdirectory.entity.Article;
import com.clicdirectory.global.ShellExecutor;
import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

import static javafx.beans.binding.Bindings.select;

public class AAPCrawl {
    public static void main(String[] args) {
        try {
            String html = ShellExecutor.execute("curl 'https://aamaadmiparty.org/category/press-release/' -H 'Connection: keep-alive' -H 'Cache-Control: max-age=0' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' -H 'Cookie: ' --compressed");
            Document x = Jsoup.parse(html,"https://aamaadmiparty.org/category/press-release/");
            System.out.println(x);
            ArrayList<Article> articles = new ArrayList<Article>();
            Elements articlesEle = x.body().select("article");
            for(Element element : articlesEle){
                try{
                    String img = element.select("img").first().absUrl("src");
                    String title = element.select("h2").first().text();
                    String description = element.select("p").text();
                    String link = element.select("a").first().absUrl("href");

                    Article article = new Article();
                    article.img = img;
                    article.title = title;
                    article.description = description;
                    article.link = link;
                    article.date = element.select("time").first().text();
                    articles.add(article);

                }catch (Exception e){

                }
            }

            System.out.println(new Gson().toJson(articles));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
