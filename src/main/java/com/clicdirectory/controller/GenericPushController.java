package com.clicdirectory.controller;

import com.clicdirectory.SysProperties;
import com.clicdirectory.database.GenericDB;
import com.clicdirectory.entity.NotifyToken;
import com.clicdirectory.entity.PushSetting;
import com.clicdirectory.global.FileUtility;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rishabh
 */
@Controller
@RequestMapping("/notify")
public class GenericPushController extends BaseController {

    private static HashMap<String,PushSetting> pushSettingHashMap = new HashMap<String, PushSetting>();
    private static ArrayList<PushSetting> pushSettings;

    private static void loadPush(){
        pushSettings = new Gson().fromJson(FileUtility.readFile(SysProperties.getBaseDir()+"/data/push/settings.txt"),new TypeToken<ArrayList<PushSetting>>(){}.getType());
        for(PushSetting pushSetting:pushSettings){
            pushSettingHashMap.put(pushSetting.load_path,pushSetting);
        }
    }

    static {
        loadPush();
    }

    public static ArrayList<PushSetting> getPushSettings(){
        return pushSettings;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/register")
    public
    @ResponseBody
    String handleCreateNotifications(
            ModelMap modelMap,
            HttpServletRequest request,
            HttpServletResponse httpServletResponse,
            @RequestBody NotifyToken notifyToken
            ){
        new GenericDB<NotifyToken>().addRow(com.clicdirectory.tables.NotifyToken.NOTIFY_TOKEN,notifyToken);
        return "ok";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{name}")
    public
    String pushShow(@PathVariable("name") String load_path, ModelMap modelMap, HttpServletRequest request, HttpServletResponse httpServletResponse){
        if(pushSettingHashMap.containsKey(load_path)){
            PushSetting pushSetting = pushSettingHashMap.get(load_path);
            modelMap.addAttribute( "PUSH_PAGE_NAME", pushSetting.result_page_name );
            modelMap.addAttribute( "RESULT_PAGE", pushSetting.result_page );
            modelMap.addAttribute( "PURPOSE", pushSetting.purpose );
            modelMap.addAttribute("SUBTITLE",pushSetting.subtitle);
            modelMap.addAttribute("DESCRIPTION",pushSetting.description);
            modelMap.addAttribute("MORE_DESCRIPTION",pushSetting.more_description);
            modelMap.addAttribute("BUTTON_ABOVE",pushSetting.button_above);
            modelMap.addAttribute("TITLE_COLOR",pushSetting.title_color);
            return "push/generic";
        }else {
            return TemplateController.home(modelMap,httpServletResponse,request);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/reload")
    public
    @ResponseBody String reload(ModelMap modelMap, HttpServletRequest request, HttpServletResponse httpServletResponse){
        loadPush();
        return "reloaded";
    }
}
