package com.clicdirectory.global;

import com.clicdirectory.controller.GenericPushController;
import com.clicdirectory.database.GenericDB;
import com.clicdirectory.entity.Notify;
import com.clicdirectory.entity.NotifyToken;
import com.clicdirectory.entity.PushObject;
import com.clicdirectory.entity.PushSetting;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hackme on 12/30/18.
 */
public class SendNotifications {
    public static void main(String campaignName) {
        long time = new Date().getTime();
        long lastSent = time - 1000;
        List<NotifyToken> x = (List<NotifyToken>) GenericDB.getRows(com.clicdirectory.tables.NotifyToken.NOTIFY_TOKEN, NotifyToken.class, com.clicdirectory.tables.NotifyToken.NOTIFY_TOKEN.T2.eq(campaignName).and(com.clicdirectory.tables.NotifyToken.NOTIFY_TOKEN.INVITE_CODERS.isNull().or(com.clicdirectory.tables.NotifyToken.NOTIFY_TOKEN.INVITE_CODERS.lt(lastSent))), null);

        for (NotifyToken notifyToken : x) {
            String SERVER_KEY = "AAAA0UlcOvs:APA91bE170haAOZNxfJ551Q0wzalYMOu6n_LxBXXmj54eJP6qG-yjTn6gk3o29ao6mx4vPxyq4YxP038rFd1uIH0lkxmGGNizklzxCCIbpmZAu-oA28xf1wjJSoB6WvfrFMWtu00TAQY";
            String name = "";
            try {
                if (notifyToken.t1 != null) {
                    name = StringUtils.getFormatedFirstName(notifyToken.t1);
                }
            } catch (Exception e) {
            }
            ArrayList<PushSetting> data = GenericPushController.getPushSettings();
            for(PushSetting pushSetting : data){
                if(pushSetting.purpose.equals(campaignName)){
                    Notify notify = new Notify("Hey " + name + ", "+pushSetting.purpose, pushSetting.subtitle, "https://upload.wikimedia.org/wikipedia/en/thumb/9/91/Aam_Aadmi_Party_Logo_%282015%29.jpg/180px-Aam_Aadmi_Party_Logo_%282015%29.jpg", pushSetting.result_page, true);
                    try {
                        System.out.println(ShellExecutor.execute("curl -X POST https://fcm.googleapis.com/fcm/send -H 'authorization: key=" + SERVER_KEY + "' -H 'cache-control: no-cache' -H 'content-type: application/json' -d '" + new Gson().toJson(new PushObject(
                                notifyToken.device_token
                                , new HashMap<String, String>() {{
                            this.put("custom1", "shortmints");
                        }}, notify)) + "'"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    new GenericDB<Long>().updateColumn(com.clicdirectory.tables.NotifyToken.NOTIFY_TOKEN.INVITE_CODERS, time, com.clicdirectory.tables.NotifyToken.NOTIFY_TOKEN, com.clicdirectory.tables.NotifyToken.NOTIFY_TOKEN.DEVICE_TOKEN.eq(notifyToken.device_token));
                }
            }

        }
    }

    public static void main(String[] args) {
        main("Political revolution in India has began");
    }
}
