// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

 var config = {
   apiKey: "AIzaSyA8ifE8FsvcAb_IbP6ER43qW4g9BQitnNY",
   authDomain: "notify-2a418.firebaseapp.com",
   databaseURL: "https://notify-2a418.firebaseio.com",
   projectId: "notify-2a418",
   storageBucket: "notify-2a418.appspot.com",
   messagingSenderId: "898878946043"
 };

firebase.initializeApp(config);

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  console.log(payload);

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});

self.addEventListener('notificationclick', function(event) {
  event.notification.close();
  // Do something as the result of the notification click

});